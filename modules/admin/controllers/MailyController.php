<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 12. 12. 2015
 * Time: 17:31
 */

namespace app\modules\admin\controllers;

use app\components\Application;
use app\components\Controller;
use app\mail\Email;
use app\modules\admin\models\EmailSearchForm;

/**
 * Class MailyController
 * @package app\modules\admin\controllers
 */
class MailyController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $mEmail = new EmailSearchForm();

        return $this->render('index', [
            'mEmail' => $mEmail
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionNahled($id)
    {
        /** @var Email $mEmail */
        $mEmail = Email::findOne($id);

        try {
            $output = $mEmail->vykresliEmail();
        } catch (\Exception $e) {
            Application::setFlashError("Při vykreslování mailu se vyskytla chyba.");
            $output = "Nelze vykreslit email.";
        }

        return $this->render('detail', [
            'content' => $output,
            'mEmail' => $mEmail
        ]);
    }
}