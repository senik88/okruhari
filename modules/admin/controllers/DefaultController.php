<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 13. 12. 2015
 * Time: 15:23
 */

namespace app\modules\admin\controllers;


use app\components\Controller;
use app\components\RoleAccessRule;
use app\modules\uzivatel\models\Uzivatel;
use yii\filters\AccessControl;

/**
 * Class DefaultController
 * @package app\modules\admin\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => RoleAccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [Uzivatel::ROLE_PORADATEL, Uzivatel::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * Dashboard
     */
    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }
}