<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 12. 12. 2015
 * Time: 17:36
 *
 * @var $mEmail EmailSearchForm
 */

use app\modules\admin\models\EmailSearchForm;

$this->title = Yii::$app->name . ' - Maily v systému';
$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    'Maily v systému'
);
?>

    <div class="h2-buttons">
        <h2>Maily odeslané systémem</h2>
        <div class="clearfix"></div>
    </div>

<?php
echo \yii\grid\GridView::widget([
    'dataProvider' => $mEmail->search(),
    'columns' => $mEmail->vratSloupce()
]);