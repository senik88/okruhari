<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 13. 12. 2015
 * Time: 16:32
 *
 * @var $content string
 * @var $mEmail Email
 */
use app\mail\Email;
use app\modules\admin\models\EmailSearchForm;

$title = "{$mEmail->predmet} pro {$mEmail->adresat}";

$this->title = Yii::$app->name . ' - ' . $title;
$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    ['label' => 'Maily v systému', 'url' => ['/admin/maily/index']],
    $title
);
?>

<div class="row">
    <div class="col-md-6">
        <ul>
            <li>Adresát: <?= $mEmail->adresat ?></li>
            <li>Předmět: <?= $mEmail->predmet ?></li>
            <li>Stav: <?= EmailSearchForm::itemAlias('stavy', $mEmail->stav) ?></li>
        </ul>
    </div>
    <div class="col-md-6">
        <ul>
            <li>Vloženo: <?= Yii::$app->formatter->asDatetime($mEmail->cas_vlozeni) ?></li>
            <li>Naplánováno: <?= Yii::$app->formatter->asDatetime($mEmail->cas_naplanovani) ?></li>
            <li>Odesláno: <?= Yii::$app->formatter->asDatetime($mEmail->cas_odeslani) ?></li>
        </ul>
    </div>
</div>

<hr />

<div class="row">
    <div class="col-md-12">
        Obsah mailu:
    </div>
</div>

<div class="row">
    <div class="col-md-12" id="mail-obsah">
        <?= $content ?>
    </div>
</div>