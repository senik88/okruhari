<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 13. 12. 2015
 * Time: 15:24
 */

$this->title = Yii::$app->name . ' - Vypsané termíny';
$this->params['breadcrumbs'] = array(
    'Administrace'
);
?>

<div class="h2-buttons">
    <h2>Dashboard</h2>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Statistiky</h3>
        <p>
            Statistiky pro jednotlivé organizátory, kolik vypsal termínů, celkem zaplacených plateb atp.
            Pro admina by tam mohlo být kolik je celkem registrovaných uživatelů, kolik se odeslalo emailů a určitě se ještě něco vymyslí.
        </p>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Nejnovější příhlášení</h3>
        <p>
            Posledních třeba 10 přihlášení na všechny aktuální vypsané termíny.
        </p>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Končící přihlášení</h3>
        <p>
            Přihlášení, která budou v brzké době expirovat. Dát možnost dobu expirace prodloužit?
        </p>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>Nově registrovaní uživatelé</h3>
        <p>
            Posledních 10 uživatelů, kteří se registrovali za poslední měsíc. Neočkávám, že to bude k něčemu, ale budiž :)
        </p>
    </div>
</div>