<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 15.12.2015
 * Time: 20:06
 */

namespace app\modules\admin\models;


use yii\base\Model;

/**
 * Class Soubor
 * @package app\modules\admin\models
 */
class Soubor extends Model
{
    /**
     * @var integer
     */
    public $soubor_pk;

    /**
     * @var string
     */
    public $nazev;

    /**
     * @var string
     */
    public $typ;

    /**
     * @var float
     */
    public $velikost;

    /**
     * @var string
     */
    public $hash;

    /**
     * @var string
     */
    public $popis;

    /**
     * @var integer
     */
    public $uzivatel_pk;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'soubor_pk' => "Soubor",
            'nazev' => "Název",
            'typ' => "Typ",
            'velikost' => "Velikost",
            'hash' => "Hash souboru",
            'popis' => "Popis"
        ];
    }

    /**
     * @return bool
     */
    public function uloz()
    {
        $sql = "INSERT INTO soubor (nazev, velikost, typ, \"hash\", popis, uzivatel_pk) VALUES (:na, :ve, :ty, :ha, :po, :uz) RETURNING soubor_pk";
        $params = [
            ':na' => $this->nazev,
            ':ve' => $this->velikost,
            ':ty' => $this->typ,
            ':ha' => $this->hash,
            ':po' => $this->popis,
            ':uz' => \Yii::$app->user->id
        ];

        $this->soubor_pk = \Yii::$app->db->createCommand($sql, $params)->queryScalar();

        return true;
    }

    /**
     * @param $pk
     * @return Soubor|null
     */
    public function nactiPodlePk($pk)
    {
        $sql = "SELECT * FROM soubor WHERE soubor_pk = :pk";
        $par = [
            ':pk' => $pk
        ];

        $data[$this->formName()] = \Yii::$app->db->createCommand($sql, $par)->queryOne();

        if ($this->load($data)) {
            return $this;
        } else {
            return null;
        }
    }

    public function priradTerminu($termin_pk)
    {
        $sql = "INSERT INTO termin_soubor (soubor_pk, termin_pk) VALUES (:spk, :tpk)";
        $params = [
            ':spk' => $this->soubor_pk,
            ':tpk' => $termin_pk
        ];

        \Yii::$app->db->createCommand($sql, $params)->execute();
    }

    public function vratCestu()
    {
        return \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'upload';
    }
}