<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 12. 12. 2015
 * Time: 17:34
 */

namespace app\modules\admin\models;


use app\components\ItemAliasTrait;
use app\components\SqlDataProvider;
use app\mail\Email;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Url;

class EmailSearchForm extends Model
{
    use ItemAliasTrait;

    /**
     * @var
     */
    public $adresat;

    /**
     * @var
     */
    public $stav;

    /**
     * @var
     */
    public $trida;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'adresat' => 'Adresát'
        ];
    }

    /**
     * @return SqlDataProvider
     */
    public function search()
    {
        $sql = "
            SELECT * FROM mail
        ";

        return new SqlDataProvider([
            'sql' => $sql,
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => [
                'attributes' => ['adresat', 'cas_vlozeni', 'cas_odeslani', 'stav'],
                'defaultOrder' => ['cas_vlozeni' => SORT_DESC]
            ]
        ]);
    }

    /**
     *
     */
    public function vratSloupce()
    {
        return [
            'trida' => [
                'attribute' => 'trida',
                'value' => function ($data) {
                    $trida = $data['trida'];
                    $lastPos = strrpos($trida, '\\', -1) + 1;

                    return substr($trida, $lastPos);
                }
            ],
            'adresat' => [
                'attribute' => 'adresat',
                'label' => $this->getAttributeLabel('adresat')
            ],
            'cas_vlozeni' => [
                'label' => 'Čas vložení',
                'format' => 'raw',
                'value' => function ($data) {
                    return \Yii::$app->formatter->asDatetime($data['cas_vlozeni']);
                }
            ],
            'cas_odeslani' => [
                'label' => 'Čas odeslání',
                'format' => 'raw',
                'value' => function ($data) {
                    return \Yii::$app->formatter->asDatetime($data['cas_odeslani']);
                }
            ],
            'stav' => [
                'label' => 'Stav',
                'value' => function ($data) {
                    return EmailSearchForm::itemAlias('stavy', $data['stav']);
                }
            ],
            'akce' => [
                'class' => 'app\components\columns\ActionColumn',
                'template' => '{nahled} {odeslat}',
                'buttons' => [
                    'nahled' => function ($url, $model, $index) {
                        return Html::a(
                            Html::icon('eye-open'),
                            ['/admin/maily/nahled', 'id' => $model['mail_pk']],
                            [
                                'title' => 'Zobrazit email'
                            ]
                        );
                    },
                    'odeslat' => function ($url, $model, $index) {
                        if (in_array($model['stav'], [Email::STAV_CHYBA, Email::STAV_ODESLANO, Email::STAV_NEODESLANO])) {
                            return Html::a(
                                Html::icon('send'),
                                ['/admin/maily/odeslat-znovu', 'id' => $model['mail_pk']],
                                [
                                    'title' => 'Odeslat email znovu'
                                ]
                            );
                        } else {
                            return null;
                        }
                    }
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    protected static function itemAliasData()
    {
        return [
            'stavy' => [
                Email::STAV_NOVY => 'nový',
                Email::STAV_ZPRACOVAVA_SE => 'zpracováva se',
                Email::STAV_ODESLANO => 'odesláno',
                Email::STAV_NEODESLANO => 'neodesláno',
                Email::STAV_CHYBA => 'chyba',
            ]
        ];
    }
}