<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 31. 5. 2015
 * Time: 16:19
 */

namespace app\modules\admin;


use yii\base\Module;

class AdminModule extends Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @var
     */
    public $name = "Admin";
}