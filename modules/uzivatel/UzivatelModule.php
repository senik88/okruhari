<?php

namespace app\modules\uzivatel;

use app\components\ModuleInterface;
use Yii;
use yii\base\Module;

/**
 * Class UzivatelModule
 * @package app\modules\uzivatel
 *
 * tohle by chtelo, aby implementovalo interface,
 * protoze budu chtit, aby mi modul vracel svoje menu
 */
class UzivatelModule extends Module implements ModuleInterface
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\uzivatel\controllers';

    /**
     * @var
     */
    public $name = "Uživatelé";

    /**
     * @var array
     */
    public $urlPrihlaseni = array('/site/index');

    /**
     * @var array
     */
    public $urlOdhlaseni = array('/uzivatel/default/odhlaseni');

    /**
     * @var array
     */
    public $baseUrl = array('/uzivatel/admin/index');

    /**
     * @var string salt pro hashovani hesla
     */
    public $salt = 'terminy@myto';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function vratMenuModulu()
    {
        return array(
            array('label' => 'Přidat uživatele', 'url' => array('/uzivatel/admin/pridat')),
            array('label' => 'Správa uživatelů', 'url' => array('/uzivatel/admin/index')),
            array('label' => 'Správa rolí', 'url' => '#'),
        );
    }

    public function osolHeslo($heslo)
    {
        return $heslo . $this->salt;
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/uzivatel/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'cs-CZ',
            'basePath' => '@app/modules/uzivatel/messages',
            'fileMap' => [
                    'modules/uzivatel/validation' => 'validation.php',
                    'modules/uzivatel/form' => 'form.php',
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/users/' . $category, $message, $params, $language);
    }
}
