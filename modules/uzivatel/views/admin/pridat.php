<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.10.14
 * Time: 18:04
 *
 * @var $mUzivatel app\modules\uzivatel\models\Uzivatel
 * @var $this \yii\web\View
 */

$this->title = Yii::$app->name . ' - Přidat uživatele';

$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    array('url' => array('/uzivatel/admin/index'), 'label' => 'Uživatelé'),
    'Přidat'
);
?>

<div class="h2-buttons">
    <h2>Přidat uživatele</h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array('mUzivatel' => $mUzivatel));