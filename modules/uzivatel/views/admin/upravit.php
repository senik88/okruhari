<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 17:14
 * @var $mUzivatel app\modules\uzivatel\models\Uzivatel
 * @var $this \yii\web\View
 */

$celeJmeno = $mUzivatel->prijmeni . ' ' . $mUzivatel->jmeno;

$this->title = Yii::$app->name . ' - Upravit uživatele';
$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    array('url' => array('/uzivatel/admin/index'), 'label' => 'Uživatelé'),
    "Editace - {$celeJmeno}"
);
?>

<div class="h2-buttons">
    <h2>Upravit uživatele <?= $celeJmeno ?></h2>
    <div class="clearfix"></div>
</div>

<?= $this->render('_form', array('mUzivatel' => $mUzivatel));