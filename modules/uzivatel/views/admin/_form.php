<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 11.10.14
 * Time: 21:44
 *
 * @var $mUzivatel Uzivatel
 * @var $this \yii\web\View
 */

use app\modules\uzivatel\models\Uzivatel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin(array(
    'id' => 'uzivatel-pridat-form',
//    'type' => 'horizontal',
//    'formConfig' => [
//        'labelSpan' => 3,
//        'spanSize' => ActiveForm::SIZE_SMALL
//    ]
));

$action = Yii::$app->controller->action->id;
?>
    <div class="form-fields">
        <?php
        echo $form->field($mUzivatel, 'email');

        if ($action == 'pridat') {
            echo $form->field($mUzivatel, 'heslo')->passwordInput();
            echo $form->field($mUzivatel, 'heslo_kontrola')->passwordInput();
        }

        echo $form->field($mUzivatel, 'jmeno');
        echo $form->field($mUzivatel, 'prijmeni');

        if (
            (
                $action == 'upravit'
                && $mUzivatel->smiEditovatRoleRoli($mUzivatel->role, Yii::$app->user->identity->getRole())
                && $mUzivatel->uzivatel_pk != Yii::$app->user->id
            ) || (
                $action == 'pridat'
            )
        ) {
            echo $form->field($mUzivatel, 'role')->dropDownList($mUzivatel->smiZalozitRoleRoli(Yii::$app->user->identity->getRole()));
        }

        // todo nemělo by to jít editovat pro mě...
        echo $form->field($mUzivatel, 'stav')->dropDownList(Uzivatel::itemAlias('stavy'))
        ?>
    </div>
    <div class="form-actions well">
        <?php
        echo Html::submitButton('Uložit', array(
            'class' => 'btn btn-success'
        ));
        echo Html::resetButton(($action == 'upravit' ? 'Původní hodnoty' : 'Vymazat formulář'), array(
            'class' => 'btn btn-primary'
        ));
        echo Html::a('Zrušit', array('/uzivatel/admin/index'), array(
            'class' => 'btn btn-danger'
        ));
        ?>
    </div>
<?php
ActiveForm::end();