<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 11.10.14
 * Time: 11:00
 *
 * @var $this \yii\web\View
 * @var $mUzivatel \app\modules\uzivatel\models\Uzivatel
 */

use app\modules\uzivatel\models\Uzivatel;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = Yii::$app->name . ' - Detail uživatele';

$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    array('url' => array('/uzivatel/admin/index'), 'label' => 'Uživatelé'),
    $mUzivatel->prijmeni . ' ' . $mUzivatel->jmeno
);
?>
<div class="h2-buttons">
    <h2>Detail uživatele <?= $mUzivatel->prijmeni ?> <?= $mUzivatel->jmeno ?></h2>
    <?php
    if ($mUzivatel->uzivatel_pk != Yii::$app->user->id) {
        echo Html::a(
            'Upravit',
            array('/uzivatel/admin/upravit', 'id' => $mUzivatel->uzivatel_pk),
            array(
                'class' => 'btn btn-warning'
            )
        );
        echo Html::a(
            'Smazat',
            array('/uzivatel/admin/smazat', 'id' => $mUzivatel->uzivatel_pk),
            array(
                'class' => 'btn btn-danger'
            )
        );
    }
    ?>
    <div class="clearfix"></div>
</div>

<?php
echo DetailView::widget(array(
    'model' => $mUzivatel,
    'attributes' => array(
        'uzivatel_pk',
        'email' => array(
            'label' => 'E-mail',
            'value' => Html::a($mUzivatel->email, 'mailto:'.$mUzivatel->email),
            'format' => 'raw'
        ),
        'jmeno',
        'prijmeni',
        'stav' => array(
            'attribute' => 'stav',
            'value' => Uzivatel::itemAlias('stavy', $mUzivatel->stav)
        ),
        'cas_registrace',
        'cas_prihlaseni'
    )
));