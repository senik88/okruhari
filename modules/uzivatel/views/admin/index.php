<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 10.10.14
 * Time: 16:53
 *
 * @var $this \yii\web\View
 * @var $mUzivatel \app\modules\uzivatel\models\Uzivatel
 */

use app\modules\uzivatel\models\Uzivatel;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::$app->name . ' - Správa uživatelů';

$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    'Uživatelé'
);

$provider = $mUzivatel->search();

?>
<div class="h2-buttons">
    <h2>Správa uživatelů</h2>
    <?php
    echo Html::a(
        'Přidat nového',
        array('/uzivatel/admin/pridat'),
        array(
            'class' => 'btn btn-success'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?php
$form = ActiveForm::begin(array(
    'id' => 'uzivatel-search-form',
    'action' => array('/uzivatel/admin/index'),
    'method' => 'get',
//    'type' => 'horizontal',
    'options' => array(
        'class' => 'well'
    ),
//    'formConfig' => ['labelSpan' => 3, 'spanSize' => ActiveForm::SIZE_SMALL]
));
?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mUzivatel, 'email', array()) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mUzivatel, 'jmeno', array())->label('Jméno / nick') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($mUzivatel, 'stav', array())->dropDownList(
                Uzivatel::itemAlias('stavy')
                , array(
                    'title' => 'Všechny / vyberte',
                    'multiple' => 'multiple'
                )
            ) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($mUzivatel, 'role', array())->dropDownList(
                Uzivatel::itemAlias('role')
                , array(
                    'title' => 'Všechny / vyberte',
                    'multiple' => 'multiple'
                )
            ) ?>
        </div>
    </div>

    <div class="form-actions">
        <?php
        echo Html::submitButton('Filtrovat', array('class' => 'btn btn-success'));
        echo Html::submitButton('Vymazat filtr', array('class' => 'btn btn-warning'));
        ?>
    </div>


<?php
ActiveForm::end();
?>

<?= GridView::widget(array(
    'dataProvider' => $provider,
    'columns' => $mUzivatel->vratSloupce(),
//    'resizableColumns' => false,
//    'export' => false
)) ?>