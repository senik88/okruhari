<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 17:28
 *
 * @var $this View
 * @var $mRegistraceForm RegistraceForm
 */

use app\modules\uzivatel\forms\RegistraceForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = Yii::$app->name . ' - Registrace';
$this->params['breadcrumbs'] = array(
    'Registrace'
);

?>

<div class="h2-buttons">
    <h2>Registrace</h2>
    <div class="clearfix"></div>
</div>

<?php
$form = ActiveForm::begin(array(
    'id' => 'registrace-form',
    'layout' => 'horizontal',
));

?>
    <div class="form-fields">
        <?php
        echo $form->field($mRegistraceForm, 'email');
        echo $form->field($mRegistraceForm, 'heslo')->passwordInput();
        echo $form->field($mRegistraceForm, 'heslo2')->passwordInput();
        echo $form->field($mRegistraceForm, 'jmeno');
        echo $form->field($mRegistraceForm, 'prijmeni');
        echo $form->field($mRegistraceForm, 'nick');
        echo $form->field($mRegistraceForm, 'informovatTerminy')->checkbox([
            'checked' => 'checked'
        ]);
        echo $form->field($mRegistraceForm, 'informovatNovinky')->checkbox();
        ?>
    </div>
    <div class="form-actions well">
        <?php
        echo Html::submitButton('Registrovat', array(
            'class' => 'btn btn-success'
        ));
//        echo Html::resetButton('Vymazat formulář', array(
//            'class' => 'btn btn-primary'
//        ));
//        echo Html::a('Zrušit', array('/uzivatel/admin/index'), array(
//            'class' => 'btn btn-danger'
//        ));
        ?>
    </div>
<?php
ActiveForm::end();