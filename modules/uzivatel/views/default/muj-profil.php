<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 6.4.2015
 * Time: 13:50
 *
 * @var $mUzivatel Uzivatel
 * @var $mPlatba Platba
 */

use app\modules\terminy\models\Platba;
use app\modules\uzivatel\models\Uzivatel;
use yii\bootstrap\Html;
use yii\widgets\DetailView;

$this->title = Yii::$app->name . ' - Můj profil';

$this->params['breadcrumbs'] = array(
    'Můj profil'
);
?>
    <div class="h2-buttons">
        <h2>Můj profil</h2>
        <?php
        echo Html::a(
            'Změnit heslo',
            array('/uzivatel/default/zmena-hesla'),
            array(
                'class' => 'btn btn-info'
            )
        );
        echo Html::a(
            'Upravit',
            array('/uzivatel/admin/upravit', 'id' => $mUzivatel->uzivatel_pk),
            array(
                'class' => 'btn btn-warning'
            )
        );
        ?>
        <div class="clearfix"></div>
    </div>

<?php
echo \yii\bootstrap\Tabs::widget([
    'items' => [
        [
            'label' => 'Přehled',
            'content' => $this->render('muj-profil-tab-prehled', [
                'mUzivatel' => $mUzivatel
            ]),
            'options' => [
                'id' => 'prehled',
            ]
        ],
        [
            'label' => 'Termíny',
            'content' => $this->render('muj-profil-tab-terminy', [
                'mPlatba' => $mPlatba
            ]),
            'options' => [
                'id' => 'terminy',
            ]
        ],
        [
            'label' => 'Historie účtu',
            'content' => $this->render('muj-profil-tab-historie', [
                'mUzivatel' => $mUzivatel
            ]),
            'options' => [
                'id' => 'historie',
            ]
        ],
    ]
]);