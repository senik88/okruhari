<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 5.10.14
 * Time: 13:59
 *
 * @var $model \app\modules\uzivatel\models\ZapomenuteHesloForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::$app->name . ' - Zapomenuté heslo';
?>
<div id="zapomenute-heslo">
    <div class="h2-buttons">
        <h2>Zapomenuté heslo</h2>
        <div class="clearfix"></div>
    </div>

    <?php
    $form = ActiveForm::begin(
        array(
            'id' => 'zapomenute-heslo-form',
            'layout' => 'horizontal'
        )
    ); ?>
    <div class="form-fields">
        <?= $form->field($model, 'email')->textInput() ?>
    </div>

    <div class="form-actions">
        <?= Html::submitButton('Odeslat', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Zpět', Yii::$app->getUrlManager()->createAbsoluteUrl('/uzivatel/default/prihlaseni'), ['class' => 'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>