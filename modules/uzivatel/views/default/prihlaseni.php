<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 5.10.14
 * Time: 13:59
 *
 * @var $mUzivatel \app\modules\uzivatel\models\Uzivatel
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::$app->name . ' - Přihlášení';

$this->params['breadcrumbs'] = array(
    'Přihlášení'
);
?>

<div id="prihlaseni">
    <div class="h2-buttons">
        <h2>Přihlášení do ISS VoPlasTo</h2>
        <div class="clearfix"></div>
    </div>

    <?php
    $form = ActiveForm::begin([
        'id' => 'prihlaseni-form',
        'layout' => 'horizontal',
        'enableClientValidation' => false
    ]); ?>
    <div class="form-fields">
        <?= $form->field($mUzivatel, 'email')->textInput() ?>
        <?= $form->field($mUzivatel, 'heslo')->passwordInput() ?>
        <?= $form->field($mUzivatel, 'zapamatovat')->checkbox() ?>
    </div>

    <?= Html::a('Zapomenuté heslo', Yii::$app->getUrlManager()->createAbsoluteUrl('/uzivatel/default/zapomenute-heslo'), ['class' => 'btn btn-link']) ?>

    <div class="form-actions well">
        <?= Html::submitButton('Přihlásit', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>