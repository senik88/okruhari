<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 25.12.2015
 * Time: 23:54
 *
 * @var Uzivatel $mUzivatel
 */

use app\modules\uzivatel\models\Uzivatel;
use yii\bootstrap\Html;
use yii\widgets\DetailView;

echo DetailView::widget(array(
    'model' => $mUzivatel,
    'attributes' => array(
        'uzivatel_pk',
        'email' => array(
            'label' => 'E-mail',
            'value' => Html::a($mUzivatel->email, 'mailto:'.$mUzivatel->email),
            'format' => 'raw'
        ),
        'jmeno',
        'prijmeni',
        'stav' => array(
            'attribute' => 'stav',
            'value' => Uzivatel::itemAlias('stavy', $mUzivatel->stav)
        ),
        'cas_registrace' => array(
            'attribute' => 'cas_registrace',
            'value' => Yii::$app->formatter->asDatetime($mUzivatel->cas_registrace)
        ),
        'cas_prihlaseni' => array(
            'attribute' => 'cas_prihlaseni',
            'value' => Yii::$app->formatter->asDatetime($mUzivatel->cas_prihlaseni)
        )
    )
));