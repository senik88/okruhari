<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 25.12.2015
 * Time: 23:56
 *
 * @var Platba $mPlatba
 */


use app\modules\terminy\models\Platba;
use app\modules\terminy\models\TerminPrihlaseni;
use yii\bootstrap\Html;
?>

<p>
    Platby super, ale tady by to chtělo výpis všech přihlášení, ne jen plateb.
    Takže udělat obrácený JOIN (termin_prihlaseni->platba) a metodu přesunout jinam.
</p>

<?php
echo \yii\grid\GridView::widget([
    'dataProvider' => $mPlatba->vratMojePlatby(),
    'columns' => [
        'termin' => [
            'label' => 'Termín',
            'value' => function ($data) {
                return date('j.n.Y', $data['termin_datum_u']);
            }
        ],
        'okruh' => [
            'label' => 'Okruh',
            'value' => function ($data) {
                return 'Vysoké Mýto';
            }
        ],
        'castka' => [
            'attribute' => 'castka',
            'label' => $mPlatba->getAttributeLabel('castka'),
            'value' => function ($data) {
                return Yii::$app->formatter->asCurrency($data['castka']);
            },
            'contentOptions' => [
                'class' => 'price-column'
            ]
        ],
        'kod' => [
            'attribute' => 'kod',
            'label' => $mPlatba->getAttributeLabel('kod')
        ],
        'stav-platby' => [
            'label' => 'Stav platby',
            'value' => function ($data) {
                switch ($data['stav']) {
                    case Platba::STAV_ZAPLACENA:
                    case Platba::STAV_PREVEDENA:
                        /*return sprintf("%s, %s: %s", $data['stav'], $data['datum'], $data['castka_zaplacena']);*/
                        return sprintf(
                            "%s: %s",
                            Platba::itemAlias('stavy', $data['stav']),
                            Yii::$app->formatter->asDatetime($data['datum'])
                        );
                        break;
                    default:
                        return Platba::itemAlias('stavy', $data['stav']);
                }
            }
        ],
        'stav-prihlaseni' => [
            'label' => 'Stav',
            'value' => function ($data) {
                return TerminPrihlaseni::itemAlias('stavy', $data['stav_prihlaseni']);
            }
        ],
        'akce' => [
            'class' => 'app\components\columns\ActionColumn',
            'template' => '{kredity}',
            'buttons' => [
                'kredity' => function ($url, $model, $index)
                {
                    $button = null;
                    if($model['lze_pouzit_kredity'] == 1) {
                        $button = Html::a(
                            Html::icon('ok'),
                            ['/terminy/default/uplatnit-kredity', 'id' => $model['termin_prihlaseni_pk']],
                            [
                                'title' => 'Uplatnit kredity'
                            ]
                        );
                    }
                    return $button;
                }
            ]
        ]
    ]
]);