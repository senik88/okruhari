<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 6.4.2015
 * Time: 14:05
 *
 * @var $mZmenaHeslaForm ZmenaHeslaForm
 */

use app\modules\uzivatel\models\ZmenaHeslaForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::$app->name . ' - Změna hesla';

$this->params['breadcrumbs'] = array(
    ['url' => ['/uzivatel/default/muj-profil'], 'label' => 'Můj profil'],
    'Změna hesla'
);

$form = ActiveForm::begin([
    'method' => 'POST',
    'id' => 'zmena-hesla-form',
//    'type' => 'horizontal',
//    'formConfig' => array(
//        'labelSpan' => 3,
//        'deviceSize' => ActiveForm::SIZE_SMALL
//    )
]);
?>

<div class="form-fields">
    <?= $form->field($mZmenaHeslaForm, 'stare')->passwordInput() ?>
    <?= $form->field($mZmenaHeslaForm, 'nove')->passwordInput() ?>
    <?= $form->field($mZmenaHeslaForm, 'kontrola')->passwordInput() ?>
</div>

<div class="form-actions well">
    <?= Html::submitButton('Uložit', ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Zrušit', Yii::$app->getUrlManager()->createAbsoluteUrl('uzivatel/default/muj-profil'), ['class' => 'btn btn-danger']) ?>
</div>

<?php
ActiveForm::end();