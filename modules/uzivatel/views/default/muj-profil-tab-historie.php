<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 25.12.2015
 * Time: 23:54
 *
 * @var Uzivatel $mUzivatel
 */

use app\modules\uzivatel\models\Uzivatel;
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $mUzivatel->vratHistoriiUctu(),
    'columns' => [
        'datum' => [
            'attribute' => 'datum',
            'value' => function ($data) {
                return Yii::$app->formatter->asDatetime($data['datum']);
            }
        ],
        'hodnota' => [
            'attribute' => 'hodnota',
            'value' => function ($data) {
                return Yii::$app->formatter->asCurrency($data['hodnota']);
            },
            'contentOptions' => [
                'class' => 'price-column'
            ]
        ],
        'popis',
        'vlozil_pk' => [
            'attribute' => 'vlozil_pk',
            'label' => 'Vložil',
            'value' => function ($data) {
                return $data['vlozil'];
            }
        ]
    ]
]);