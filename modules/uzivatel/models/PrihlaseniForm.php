<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 11.10.14
 * Time: 21:17
 */

namespace app\modules\uzivatel\models;


use yii\base\Model;

class PrihlaseniForm extends Model {

    /**
     * @var
     */
    public $email;

    /**
     * @var
     */
    public $heslo;

    /**
     * @var
     */
    public $pamatovat;

} 