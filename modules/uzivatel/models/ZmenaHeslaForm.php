<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 6.4.2015
 * Time: 14:16
 */

namespace app\modules\uzivatel\models;
use yii\base\Model;


/**
 * Class ZmenaHeslaForm
 * @package app\modules\uzivatel\models
 */
class ZmenaHeslaForm extends Model
{
    public $stare;

    public $nove;

    public $kontrola;

    public function rules() {
        return [
            [['stare', 'nove', 'kontrola'], 'required'],
            ['stare', 'stareHesloValidator'],
            [['kontrola'], 'compare', 'compareAttribute' => 'nove']
        ];
    }

    public function attributeLabels()
    {
        return [
            'stare' => 'Staré heslo',
            'nove' => 'Nové heslo',
            'kontrola' => 'Kontrola hesla'
        ];
    }

    /**
     * @return bool
     */
    public function uloz()
    {
        /** @var Uzivatel $mUzivatel */
        $mUzivatel = Uzivatel::findOne(\Yii::$app->user->id);
        $mUzivatel->heslo = \Yii::$app->security->generatePasswordHash($this->nove . \Yii::$app->getModule('uzivatel')->salt);

        return $mUzivatel->save(false, ['heslo']);
    }

    /**
     * @param $attribute
     * @param $params
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function stareHesloValidator($attribute, $params)
    {
        $db = \Yii::$app->db;
        $command = $db->createCommand("select heslo from uzivatel where uzivatel_pk = :pk");
        $command->bindValues([
            'pk' => \Yii::$app->user->id,
        ]);

        $stare = $command->queryScalar();

        if (!\Yii::$app->security->validatePassword($this->$attribute. \Yii::$app->getModule('uzivatel')->salt, $stare)) {
            $this->addError($attribute, 'Neplatné staré heslo');
        }
    }
}