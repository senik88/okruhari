<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 25.10.14
 * Time: 14:18
 */

namespace app\modules\uzivatel\models;

use yii\base\Exception;
use yii\base\Model;
use Yii;

/**
 * Class ZapomenuteHesloZmenaForm
 * @package app\modules\uzivatel\models
 */
class ZapomenuteHesloZmenaForm extends Model
{

    /**
     * @var Uzivatel
     */
    protected $_uzivatel;

    /**
     * @var
     */
    public $heslo;

    /**
     * @var
     */
    public $kontrola;

    /**
     * @param string $klic
     * @param array $params
     * @throws \yii\base\Exception
     */
    public function __construct($klic, $params = array())
    {
        parent::__construct($params);

        $mUzivatel = Uzivatel::findOne(array('validacni_klic' => $klic));
        if ($mUzivatel == false) {
            throw new Exception('Uzivatel nenalezen!');
        } else {
            $this->_uzivatel = $mUzivatel;
        }
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array(array('heslo', 'kontrola'), 'required')
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'heslo' => 'Nové heslo',
            'kontrola' => 'Kontrola hesla'
        );
    }

    /**
     * Ulozi k uzivateli nove heslo
     * @return bool
     */
    public function ulozNoveHeslo()
    {
        $heslo = $this->heslo . Yii::$app->getModule('uzivatel')->salt;
        $this->_uzivatel->heslo = Yii::$app->getSecurity()->generatePasswordHash($heslo);
        $this->_uzivatel->validacni_klic = md5(uniqid());
        return $this->_uzivatel->save(false);
    }

} 