<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 25.10.14
 * Time: 14:18
 */

namespace app\modules\uzivatel\models;

use yii\base\Model;

/**
 * Class ZapomenuteHesloForm
 * @package app\modules\uzivatel\models
 */
class ZapomenuteHesloForm extends Model
{
    /**
     * @var
     */
    public $email;

    /**
     * @var
     */
    public $validacniKlic;

    /**
     * @return array
     */
    public function rules()
    {
        return array(
            array('email', 'required'),
            array('email', 'email'),
            array('email', 'existujiciUcetValidator'),
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'email' => 'E-mail'
        );
    }

    /**
     * @return bool
     */
    public function existujiciUcetValidator()
    {
        /** @var Uzivatel $mUzivatel */
        $mUzivatel = Uzivatel::findOne(array('email' => trim(strtolower($this->email))));
        if (is_null($mUzivatel)) {
            $this->addError('email', 'Neplatné uživatelské jméno!');
            return false;
        }

        $mUzivatel->validacni_klic = md5(uniqid());
        if ($mUzivatel->save(false)) {
            $this->validacniKlic = $mUzivatel->validacni_klic;
            return true;
        } else {
            $this->addError('email', 'Něco se pokazilo, zkuste znovu později.');
            return false;
        }
    }

}