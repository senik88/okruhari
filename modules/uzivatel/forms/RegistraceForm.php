<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 17:31
 */

namespace app\modules\uzivatel\forms;


use app\modules\uzivatel\models\Uzivatel;
use yii\base\Model;

/**
 * Class RegistraceForm
 * @package app\modules\uzivatel\forms
 */
class RegistraceForm extends Model
{
    /**
     * @var
     */
    public $jmeno;

    /**
     * @var
     */
    public $prijmeni;

    /**
     * @var
     */
    public $email;

    /**
     * @var
     */
    public $heslo;

    /**
     * @var
     */
    public $heslo2;

    /**
     * @var
     */
    public $zaslatEmail;

    /**
     * @var
     */
    public $nick;

    /**
     * @var
     */
    public $informovatTerminy = 1;

    /**
     * @var
     */
    public $informovatNovinky = 0;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['jmeno', 'prijmeni', 'email', 'heslo', 'heslo2', 'nick'], 'required'],
            [['zaslatEmail', 'nick', 'informovatNovinky', 'informovatTerminy'], 'safe'],
            ['email', 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jmeno' => 'Jméno',
            'prijmeni' => 'Příjmení',
            'email' => 'E-mail',
            'heslo' => 'Heslo',
            'heslo2' => 'Kontrola hesla',
            'nick' => 'Přezdívka',
            'informovatTerminy' => 'Informovat mě o termínech',
            'informovatNovinky' => 'Informovat mě o novinkách'
        ];
    }

    /**
     * todo odchytavat chyby pri odesilani emailu
     */
    public function save()
    {
        $data = [
            'jmeno' => $this->jmeno,
            'prijmeni' => $this->prijmeni,
            'email' => $this->email,
            'cas_registrace' => date('Y-m-d H:i:s', time()),
            'stav' => Uzivatel::STAV_REGISTRACE,
            'validacni_klic' => md5($this->email . time()),
            'heslo' => \Yii::$app->security->generatePasswordHash($this->heslo . \Yii::$app->getModule('uzivatel')->salt),
            'role' => Uzivatel::ROLE_USER,
            'nick' => $this->nick
        ];

        $mUzivatel = new Uzivatel();

        if ($mUzivatel->load($data, '')) {

            //$transaction = \Yii::$app->db->beginTransaction();

            try {

            } catch (\Exception $e) {

            }

            if ($mUzivatel->save(false)) {
                // terminy chci vzdy, proto se registruju :) // no mozna dam radeji na vyber
                $mUzivatel->nastavOdber($this->informovatTerminy, $this->informovatNovinky);
                // odeslu aktivaci
                $mUzivatel->odesliAktivacniEmail();

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}