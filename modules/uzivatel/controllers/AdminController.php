<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 6.10.14
 * Time: 19:23
 */

namespace app\modules\uzivatel\controllers;


use app\components\Application;
use app\components\RoleAccessRule;
use app\modules\uzivatel\models\Uzivatel;
use app\components\Controller;
use app\widgets\UzivatelWidget;
use Yii;
use yii\filters\AccessControl;
use yii\web\HttpException;

/**
 * Class AdminController
 * @package app\modules\uzivatel\controllers
 *
 * controller zajistujici kompletni spravu uzivatelu
 */
class AdminController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => RoleAccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'pridat', 'detail', 'upravit', 'smazat'],
                        'roles' => [Uzivatel::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $mUzivatel = new Uzivatel(['scenario' => 'search']);

        $get = Yii::$app->request->get();
        if (!empty($get)) {
            $mUzivatel->load($get);
        }

        return $this->render('index', array(
            'mUzivatel' => $mUzivatel
        ));
    }

    /**
     * @param $id
     * @throws \yii\web\HttpException
     * @return string
     */
    public function actionDetail($id)
    {
        $mUzivatel = Uzivatel::findOne($id);

        if ($mUzivatel == null) {
            throw new HttpException(404, 'Uživatel nenalezen');
        }

        return $this->render('detail', array(
            'mUzivatel' => $mUzivatel
        ));
    }

    /**
     *
     */
    public function actionPridat()
    {
        $mUzivatel = new Uzivatel();

        $post = Yii::$app->request->post();
        if ($mUzivatel->load($post)) {
            $mUzivatel->cas_registrace = date('Y-m-d H:i:s', time());
            $mUzivatel->stav = Uzivatel::STAV_AKTIVNI;
            $mUzivatel->validacni_klic = md5($mUzivatel->email . $mUzivatel->cas_registrace);

            if ($mUzivatel->validate()) {
                $heslo = $mUzivatel->heslo . Yii::$app->getModule('uzivatel')->salt;
                $mUzivatel->heslo = Yii::$app->security->generatePasswordHash($heslo);
                if ($mUzivatel->save(false)) {
                    Application::setFlashSuccess('Uživatel uložen');
                    return $this->redirect(array('/uzivatel/admin/index'));
                }
            }
        } else {

        }

        return $this->render('pridat', array(
            'mUzivatel' => $mUzivatel
        ));
    }

    /**
     * @param $id
     * @throws \yii\web\HttpException
     * @return string
     */
    public function actionUpravit($id)
    {
        /** @var Uzivatel $mUzivatel */
        $mUzivatel = Uzivatel::findOne($id);
        $mUzivatel->scenario = 'upravit';

        if ($mUzivatel == null) {
            throw new HttpException(404, 'Uživatel nenalezen');
        }

        $post = Yii::$app->request->post();

        if ($mUzivatel->load($post) && $mUzivatel->validate()) {
            if ($mUzivatel->save()) {
                Application::setFlashSuccess("Uživatel {$mUzivatel->prijmeni} {$mUzivatel->jmeno} upraven");
                return $this->redirect(array('/uzivatel/admin/index'));
            } else {

            }
        } else {

        }

        return $this->render('upravit', array(
            'mUzivatel' => $mUzivatel
        ));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionSmazat($id)
    {
        /** @var Uzivatel $mUzivatel */
        $mUzivatel = Uzivatel::findOne($id);

        if ($mUzivatel == null) {
            throw new HttpException(404, 'Uživatel nenalezen');
        }

        if ($mUzivatel->delete()) {
            Application::setFlashSuccess('Uživatel smazán');
        } else {
            Application::setFlashError('Uživatele se nepodařilo smazat');
        }

        return $this->redirect(array('/uzivatel/admin/index'));
    }

}