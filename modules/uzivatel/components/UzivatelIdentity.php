<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 5.10.14
 * Time: 18:54
 */

namespace app\modules\uzivatel\components;


use app\components\Nastaveni;
use app\modules\uzivatel\models\Uzivatel;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\base\Object;
use yii\web\IdentityInterface;
use Yii;

/**
 * Class UzivatelIdentity
 * @package app\modules\uzivatel\components
 */
class UzivatelIdentity extends Object implements IdentityInterface
{

    const ERROR_NONE = 0;
    const ERROR_LOGIN = -1;
    const ERROR_PASS = -2;
    const ERROR_REG = -3;
    const ERROR_PARAM = -5;
    const ERROR_UNKNOWN = -10;

    /**
     * @var string email
     */
    public $login;

    /**
     * @var
     */
    public $heslo;

    /**
     * @var string uzivatel_pk
     */
    public $id;

    /**
     * @var string role uzivatele
     */
    public $role;

    /**
     * @var array
     */
    protected $_errors = array();

    /**
     * @var Uzivatel
     */
    protected $_user;

    /**
     * @var bool
     */
    protected $_isAdmin = false;

    /**
     * @param string $login
     * @param string $heslo
     * @param array $config
     */
    public function __construct($login, $heslo, $config = array())
    {
        parent::__construct($config);

        $this->login = $login;
        $this->heslo = $heslo . Yii::$app->getModule('uzivatel')->salt;
    }

    /**
     * @return int
     */
    public function authenticate()
    {
        /** @var Uzivatel $mUzivatel */
        $mUzivatel = Uzivatel::findOne(array(
            'email' => $this->login,
            //'stav' => Uzivatel::STAV_AKTIVNI
        ));

        try {
            if ($mUzivatel == null) {
                return self::ERROR_LOGIN;
            } else if ($mUzivatel->stav != Uzivatel::STAV_AKTIVNI) {
                return self::ERROR_REG;
            } else if (!Yii::$app->security->validatePassword($this->heslo, $mUzivatel->heslo)) {
                return self::ERROR_PASS;
            } else {
                $this->_user = $mUzivatel;

                $mUzivatel->cas_prihlaseni = date('Y.m.d H:i:s', time());
                $mUzivatel->save(false);

                $this->setIsAdmin();

                return self::ERROR_NONE;
            }
        } catch (InvalidParamException $e) {
            $this->_errors[] = array('code' => $e->getCode(), 'message' => $e->getMessage());
            return self::ERROR_PARAM;
        } catch (Exception $e) {
            $this->_errors[] = array('code' => $e->getCode(), 'message' => $e->getMessage());
            return self::ERROR_UNKNOWN;
        }
    }

    /**
     * Vrati informaci, zda je prihlaseny uzivatel administrator.
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->_isAdmin;
    }

    /**
     * Nastavi prihlaseneho jako administratora
     */
    public function setIsAdmin()
    {
        $this->_isAdmin = in_array($this->_user->email, Yii::$app->params['admini']);
    }

    /**
     * Vrati posledni zapsany error do UzivatelIdentity
     * @return array
     */
    public function getLastError()
    {
        if (empty($this->_errors)) {
            return array();
        } else {
            $lastKey = end(array_keys($this->_errors));
            return $this->_errors[$lastKey];
        }
    }

    /**
     * Finds an identity by the given ID.
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        // TODO: odladit
        /** @var Uzivatel $mUzivatel */
        $mUzivatel = Uzivatel::findOne($id);

        if ($mUzivatel != null) {
            $identity = new self($mUzivatel->email, $mUzivatel->heslo);
            $identity->_user = $mUzivatel;
            $identity->setIsAdmin();
            return $identity;
        } else {
            return null;
        }
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
        var_dump($token); die;
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->_user->uzivatel_pk;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->_user->validacni_klic;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $authKey == $this->getAuthKey();
    }

    /**
     * @return Uzivatel
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->_user->role;
    }
}