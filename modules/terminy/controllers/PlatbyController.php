<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 11. 12. 2015
 * Time: 17:38
 */

namespace app\modules\terminy\controllers;


use app\components\Application;
use app\components\Controller;
use app\modules\terminy\models\Platba;
use app\modules\terminy\models\PlatbaSearchForm;
use app\modules\terminy\models\TerminPrihlaseni;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class PlatbyController
 * @package app\modules\terminy\controllers
 */
class PlatbyController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $mPlatbaSearch = new PlatbaSearchForm();

        return $this->render('index', [
            'mPlatbaSearch' => $mPlatbaSearch
        ]);
    }

    /**
     * Potvrzeni prijeti platby, bud je to GET pozadavek (vyplnene pk (= id)) nebo POST (odeslani formulare z modalu)
     *
     * @param integer|null $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionPotvrdit($id = null)
    {
        if ($id != null) {
            /** @var Platba $mPlatba */
            $mPlatba = Platba::findOne($id);
        } else if ($id == null && \Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post();
            /** @var Platba $mPlatba */
            $mPlatba = Platba::findOne(['kod' => $post['Platba']['kod']]);
        } else {
            throw new HttpException(500, "Neplatný požadavek");
        }

        if (null == $mPlatba) {
            throw new HttpException(404, "Platba nenalezena");
        }

        if ($mPlatba->potvrdPrijeti()) {
            Application::setFlashSuccess("Přijetí platby bylo potvrzeno.");
        } else {
            Application::setFlashError("Chyba při potvrzování platby, opakujte akci později.");
        }

        if (strpos(\Yii::$app->request->referrer, 'terminy/platby')) {
            $url = Url::to(['/terminy/platby/detail', 'id' => $mPlatba->platba_pk]);
        } else {
            $url = \Yii::$app->request->referrer;
        }

        return $this->redirect($url);
    }
}