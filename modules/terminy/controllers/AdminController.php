<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 22:56
 */

namespace app\modules\terminy\controllers;


use app\components\RoleAccessRule;
use app\modules\terminy\models\Platba;
use app\modules\terminy\models\PlatbaSearchForm;
use app\modules\terminy\models\Termin;
use app\modules\uzivatel\models\Uzivatel;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class AdminController
 * @package app\modules\terminy\controllers
 */
class AdminController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => RoleAccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'pridat', 'zrusit', 'platby'],
                        'roles' => [Uzivatel::ROLE_PORADATEL, Uzivatel::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $mTermin = new Termin();

        return $this->render('index', [
            'mTermin' => $mTermin
        ]);
    }

    /**
     * @return string
     */
    public function actionPridat()
    {
        $mTermin = new Termin([
            'scenario' => Termin::SCENARIO_PRIDAT
        ]);

        $post = \Yii::$app->request->post();

        if (!empty($post)) {

            $load = $mTermin->load($post);

            $mTermin->soubory = UploadedFile::getInstances($mTermin, 'soubory');

            if ($mTermin->validate()) {
                if ($mTermin->uloz()) {
                    return $this->redirect(['/terminy/admin/index']);
                }
            }
        }

        return $this->render('pridat', [
            'mTermin' => $mTermin
        ]);
    }

    /**
     * @param $id integer termin_pk
     */
    public function actionZrusit($id)
    {

    }

    /**
     * @param $termin integer termin_pk
     * @return string
     * @throws HttpException
     */
    public function actionPlatby($termin)
    {
        /** @var Termin $mTermin */
        $mTermin = (new Termin())->nactiPodlePk($termin);

        if (null == $mTermin) {
            throw new HttpException(404, "Neexistující termín");
        }

        $mPlatba = new PlatbaSearchForm(['scenario' => PlatbaSearchForm::SCENARIO_SUBMIT]);
        $mPlatba->termin_pk = $mTermin->termin_pk;

        $get = \Yii::$app->request->get();
        if (!empty($get)) {
            $mPlatba->load($get);
        }

        $aDetaily = $mPlatba->nactiOcekavaneZaplaceneTerminu($termin);

        return $this->render('platby', [
            'mTermin' => $mTermin,
            'mPlatba' => $mPlatba,
            'aDetaily' => $aDetaily
        ]);
    }
}