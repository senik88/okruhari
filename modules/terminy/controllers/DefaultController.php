<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 22:55
 */

namespace app\modules\terminy\controllers;


use app\components\Application;
use app\components\RoleAccessRule;
use app\modules\terminy\models\Platba;
use app\modules\terminy\models\Termin;
use app\modules\terminy\models\TerminPrihlaseni;
use app\modules\uzivatel\models\Uzivatel;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\terminy\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['prihlasit', 'odhlasit'],
                'ruleConfig' => [
                    'class' => RoleAccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['prihlasit', 'odhlasit'],
                        'roles' => [Uzivatel::ROLE_USER, Uzivatel::ROLE_PORADATEL, Uzivatel::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $mTermin = new Termin();
        $mTermin->stav = Termin::STAV_VEREJNY;

        return $this->render('index', [
            'mTermin' => $mTermin
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionDetail($id)
    {
        $mTermin = (new Termin())->nactiPodlePk($id);

        if (null == $mTermin || ($mTermin->stav == Termin::STAV_NOVY && !\Yii::$app->user->getIsAdmin())) {
            Application::setFlashWarning("Takový termín neexistuje.");
            return $this->redirect(['/terminy/default/index']);
        }

        $user = \Yii::$app->user;
        $mTerminPrihlaseni = null;
        $mPlatba = null;
        $hash = TerminPrihlaseni::sestavHash($mTermin->termin_pk, $user->id);

        if (!$user->isGuest) {
            /** @var TerminPrihlaseni $mTerminPrihlaseni */
            $mTerminPrihlaseni = TerminPrihlaseni::findOne([
                'termin_pk' => $mTermin->termin_pk,
                'uzivatel_pk' => $user->id
            ]);

            if (null != $mTerminPrihlaseni) {
                /** @var Platba $mPlatba */
                $mPlatba = Platba::findOne(['platba_pk' => $mTerminPrihlaseni->platba_pk]);
            }
        }

        return $this->render('detail', [
            'mTermin' => $mTermin,
            'mTerminPrihlaseni' => $mTerminPrihlaseni,
            'mPlatba' => $mPlatba,
            'hash' => $hash
        ]);
    }

    /**
     * Hash jsou zatim jen base64 encoded data, nez prijdu na lepsi zpusob
     *
     * @param $hash string base64 encoded termin_pk a uzivatel_pk
     * @return \yii\web\Response
     */
    public function actionPrihlasit($hash)
    {
        if (\Yii::$app->user->isGuest) {
            Application::setFlashError("Pro tuto akci je nutné se přihlásit.");
            return $this->redirect(['/terminy/default/index']);
        }

        try {
            $mTermin = (new TerminPrihlaseni())->naplnPodleHashe($hash);
            $prihlasen = $mTermin->prihlas();
        } catch (\Exception $e) {
            \Yii::error("nepodarilo se prihlasit na termin, chyba: {$e->getMessage()}");
            Application::setFlashError('Při přihlašování se vyskytla chyba, opakuj akci později.');
            return $this->redirect(['/terminy/default/index']);
        }

        if ($prihlasen) {
            Application::setFlashSuccess("Přihlášení na termín bylo úspěšné.");
        } else {
            Application::setFlashError("Nepodařilo se přihlásit tě na termín, kontaktuj správce.");
        }

        return $this->redirect(['/terminy/default/detail', 'id' => $mTermin->termin_pk]);
    }

    /**
     * Hash jsou zatim jen base64 encoded data, nez prijdu na lepsi zpusob
     *
     * @param $hash string base64 encoded termin_pk a uzivatel_pk
     * @return \yii\web\Response
     */
    public function actionOdhlasit($hash)
    {
        if (\Yii::$app->user->isGuest) {
            Application::setFlashError("Pro tuto akci je nutné se přihlásit.");
            return $this->redirect(['/terminy/default/index']);
        }

        try {
            $mTermin = (new TerminPrihlaseni())->naplnPodleHashe($hash);
            $odhlasen = $mTermin->odhlas();
        } catch (\Exception $e) {
            \Yii::error("nepodarilo se odhlasit z terminu, chyba: {$e->getMessage()}");
            Application::setFlashError('Při odhlašování se vyskytla chyba, opakuj akci později.');
            return $this->redirect(['/terminy/default/index']);
        }

        if ($odhlasen) {
            Application::setFlashSuccess("Odhlášení z termínu bylo úspěšné.");
        } else {
            Application::setFlashError("Nepodařilo se odhlásit tě z termínu, kontaktuj správce.");
        }

        return $this->redirect(['/terminy/default/detail', 'id' => $mTermin->termin_pk]);
    }



    /**
     * Casem bych to asi predelal na ajax...
     * @param $id
     * @return \yii\web\Response
     */
    public function actionUplatnitKredity($id)
    {
        /** @var TerminPrihlaseni $mTerminPrihlaseni */
        $mTerminPrihlaseni = TerminPrihlaseni::findOne($id);

        // tady by bylo vhodné to pořešit právě přes $user->can()
        if ($mTerminPrihlaseni->uzivatel_pk != \Yii::$app->user->id) {
            Application::setFlashError("Pro tuto akci nemáte oprávnění");
            return $this->redirect([]);
        }

        if ($platba_pk = $mTerminPrihlaseni->uplatniKredity()) {
            /** @var Platba $mPlatba */
            $mPlatba = Platba::findOne($id);

            Application::setFlashSuccess(sprintf(
                "Na platbu bylo uplatněny kredity, zaplaceno %s z %s",
                \Yii::$app->formatter->asCurrency($mPlatba->castka_zaplacena),
                \Yii::$app->formatter->asCurrency($mPlatba->castka)
            ));
        } else {
            Application::setFlashError("Vyskytla se chyba, opakujte prosím akci později");
        }

        // presmeruju na referrera, protoze sem muzu dojit z vice mist
        // prehled plateb v mem uctu, modal platby v detailu terminu
        return $this->redirect(\Yii::$app->request->referrer);
    }
}