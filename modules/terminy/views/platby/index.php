<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.12.2015
 * Time: 23:59
 */

$this->title = Yii::$app->name . ' - Přehled plateb';
$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    'Platby'
);
?>

<div class="h2-buttons">
    <h2>Přehled plateb</h2>
    <div class="clearfix"></div>
</div>

<p>
    Zatím nechám být, platby lze zobrazit přes jednotlivé termíny, takže komplet přehled není zatím třeba...
</p>