<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 3. 6. 2015
 * Time: 21:23
 *
 * @var $model TerminPrihlaseni
 * @var $mUzivatel Uzivatel
 */

use app\modules\terminy\models\Platba;
use app\modules\terminy\models\TerminPrihlaseni;
use app\modules\uzivatel\models\Uzivatel;

//$mUzivatel = Uzivatel::findOne($model->uzivatel_pk);

/** @var Platba $mPlatba */
$mPlatba = \app\modules\terminy\models\Platba::findOne($model->platba_pk);
$append = null;

if ($model->stav == TerminPrihlaseni::STAV_POTVRZENY) {
    $append = \yii\bootstrap\Html::icon('ok', ['title' => 'Má zaplaceno: ' . Yii::$app->formatter->asDatetime($mPlatba->datum)]);
}

$jmeno = "{$model->uzivatel->prijmeni} {$model->uzivatel->jmeno}";

if (null != $append) {
    $jmeno .= ' ' . $append;
}

echo $jmeno;