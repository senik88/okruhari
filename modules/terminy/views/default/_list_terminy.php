<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 3. 6. 2015
 * Time: 16:41
 *
 * @var $model array
 */
use app\modules\terminy\models\Termin;
use app\modules\terminy\models\TerminPrihlaseni;
use yii\helpers\Html;

$locale = setlocale(LC_TIME, 'cs_CZ.utf8');
$user = Yii::$app->user;

$wrapperClasses = ['termin'];

if ($model['probehly'] == 1) {
    $wrapperClasses[] = 'probehly';
}
?>
<div class="<?= implode(' ', $wrapperClasses) ?>">
<table>
    <tr>
        <td class="termin-den"><?= strftime('%e', strtotime($model['cas_od'])) ?></td>
    </tr>
    <tr>
        <td class="termin-mesic"><?= mb_strtoupper(strftime('%B', strtotime($model['cas_od'])), 'UTF-8') ?></td>
    </tr>
    <tr>
        <td class="termin-cas"><?= date('H:i', strtotime($model['cas_od'])) ?> - <?= date('H:i', strtotime($model['cas_do'])) ?></td>
    </tr>
    <tr class="separator"></tr>
    <tr>
        <td>
            Kapacita
            <br>
            <?= $model['volno'] ?> / <?= $model['kapacita'] ?>
        </td>
    </tr>
    <tr class="separator"></tr>
    <tr>
        <td class="termin-buttony">
            <?php
            echo Html::a('Detail', ['/terminy/default/detail', 'id' => $model['termin_pk']], ['class' => 'btn btn-info']);

            if (!$user->isGuest && $model['stav'] != Termin::STAV_STORNO && strtotime($model['cas_od']) > time()) {
                /** @var TerminPrihlaseni $termin */
                $termin = TerminPrihlaseni::findOne([
                    'termin_pk' => $model['termin_pk'],
                    'uzivatel_pk' => $user->id
                ]);

                $hash = TerminPrihlaseni::sestavHash($model['termin_pk'], $user->id);

                if (null == $termin || $termin->stav == TerminPrihlaseni::STAV_ZRUSENY) {
                    echo Html::a('Přihlásit se', ['/terminy/default/prihlasit', 'hash' => $hash], ['class' => 'btn btn-success']);
                }

                if (null != $termin && in_array($termin->stav, [TerminPrihlaseni::STAV_NAHRADNIK, TerminPrihlaseni::STAV_POTVRZENY, TerminPrihlaseni::STAV_PRIHLASENY])) {
                    echo Html::a('Odhlásit se', ['/terminy/default/odhlasit', 'hash' => $hash], ['class' => 'btn btn-danger']);
                }
            }
            ?>
        </td>
    </tr>
</table>
</div>
<?php
setlocale(LC_TIME, $locale);