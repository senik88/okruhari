<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 22:55
 *
 * @var $mTermin Termin
 */

use app\modules\terminy\models\Termin;
use yii\widgets\ListView;

$this->title = Yii::$app->name . ' - Vypsané termíny';
$this->params['breadcrumbs'] = array(
    'Vypsané termíny'
);

echo ListView::widget([
    'id' => 'terminy-list',
    'dataProvider' => $mTermin->search(),
    'itemView' => '_list_terminy',
    'itemOptions' => [
        'class' => 'termin-wrapper col-md-3'
    ],
    'layout' => "{items}\n{pager}"
]);