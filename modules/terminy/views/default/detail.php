<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 2. 6. 2015
 * Time: 23:30
 *
 * @var $mTermin Termin
 * @var $mTerminPrihlaseni TerminPrihlaseni
 * @var $mPlatba Platba
 * @var $hash string
 */

use app\modules\terminy\models\Platba;
use app\modules\terminy\models\Pocasi;
use app\modules\terminy\models\Termin;
use app\modules\terminy\models\TerminPrihlaseni;
use app\modules\uzivatel\models\Uzivatel;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

$this->title = Yii::$app->name . ' - Nový termín';
$this->params['breadcrumbs'] = array(
    ['url' => ['/terminy/default/index'], 'label' => 'Termíny'],
    Yii::$app->formatter->asDate($mTermin->cas_od)
);
?>

<div class="h2-buttons">
    <h2>Termín <?= Yii::$app->formatter->asDate($mTermin->cas_od) ?></h2>
    <?php
    if (!Yii::$app->user->isGuest && $mTermin->stav != Termin::STAV_STORNO && strtotime($mTermin->cas_od) > time()) {
        if (null == $mTerminPrihlaseni || $mTerminPrihlaseni->stav == TerminPrihlaseni::STAV_ZRUSENY) {
            echo Html::a('Přihlásit se', ['/terminy/default/prihlasit', 'hash' => $hash], ['class' => 'btn btn-success']);
        }

        if (null != $mTerminPrihlaseni && in_array($mTerminPrihlaseni->stav, [TerminPrihlaseni::STAV_NAHRADNIK, TerminPrihlaseni::STAV_POTVRZENY, TerminPrihlaseni::STAV_PRIHLASENY])) {
            echo Html::a('Odhlásit se', ['/terminy/default/odhlasit', 'hash' => $hash], ['class' => 'btn btn-danger']);
        }
    }

    if (null != $mPlatba) {
        echo Html::a('Informace o platbě', '#', ['class' => 'btn btn-info', 'data-toggle' => 'modal', 'data-target' => '#platba-info-modal']);
    }
    ?>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php echo $mTermin->popis; ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="pocasi-wrapper">
            <?php list($predpoved, $ikona) = (new Pocasi())->vratProTermin($mTermin->termin_pk); ?>
            <h4>Předpověď počasí</h4>
            <div class="row">
                <div class="col-md-2">
                    <img src="<?= $ikona ?>" title="<?= $predpoved['weather'][0]['description'] ?>"/>
                </div>
                <div class="col-md-4">
                    <ul>
                        <li>Teploty:
                            <ul>
                                <li>Denní <?= $predpoved['temp']['day'] ?> °C</li>
                                <li>Minimální <?= $predpoved['temp']['min'] ?> °C</li>
                                <li>Maximální <?= $predpoved['temp']['max'] ?> °C</li>
                            </ul>
                        </li>
                        <li>Déšť <?= isset($predpoved['rain']) ? $predpoved['rain'] : 0 ?> mm</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul>
                        <li>Vlhkost <?= $predpoved['humidity'] ?> %</li>
                        <li>Tlak <?= $predpoved['pressure'] ?> hPa</li>
                        <li>Vítr <?= $predpoved['speed'] ?> m/s, <?= $predpoved['deg'] ?>°</li>
                        <li>Mraky <?= $predpoved['clouds'] ?> %</li>
                    </ul>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        if (is_array($mTermin->soubory) && !empty($mTermin->soubory)) {
            echo Html::tag('h4', 'Připojené soubory');
            echo Html::beginTag('ul');

            foreach ($mTermin->soubory as $mSoubor) {
                echo Html::tag('li', Html::a(
                    $mSoubor->nazev,
                    ['/site/stahnout', 'soubor' => $mSoubor->soubor_pk]
                ));
            }

            echo Html::endTag('ul');
        }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="h4-buttons">
            <h4>Přihlášení</h4>
            <?php
            // pokud jsem superadmin nebo organizator
            if (Yii::$app->user->can('spravovatPlatby', ['termin_pk' => $mTermin->termin_pk])) {
                echo Html::a(
                    'Přehled plateb ' . Html::icon('chevron-right'),
                    ['/terminy/admin/platby', 'termin' => $mTermin->termin_pk]
                );
            }
            ?>
            <div class="clearfix"></div>
        </div>
        <?php
        if (!empty($mTermin->prihlaseni)) {
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $mTermin->prihlaseni,
                'pagination' => false
            ]);

            echo \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_list_prihlaseny',
                'layout' => "{items}"
            ]);
        } else {
            echo "Zatím se nikdo nepřihlásil.";
        }
        ?>
    </div>

    <div class="col-md-6">
        <h4>Náhradníci</h4>
        <?php
        if (!empty($mTermin->nahradnici)) {
            // todo vypisovat nahradniky
        } else {
            echo "Zatím žádní náhradníci, hlaste se.";
        }
        ?>
    </div>
</div>

<?php
if (null != $mPlatba) {
    Modal::begin([
        'id' => 'platba-info-modal',
        'header' => 'Informace o platbě za ježdění'
    ]);

        echo Html::beginTag('ul');
        {
            echo Html::tag('li', 'Cílový účet: 123456789/0999');
            echo Html::tag('li', 'Variabilní symbol: ' . $mPlatba->kod);
            echo Html::tag('li', 'Očekávaná částka: ' . Yii::$app->formatter->asCurrency($mPlatba->castka));
            echo Html::tag('li', 'Zaplacená částka: ' . Yii::$app->formatter->asCurrency($mPlatba->castka_zaplacena));
            echo Html::tag('li', 'Stav: ' . Html::tag('strong', Platba::itemAlias('stavy', $mPlatba->stav)));
        }
        echo Html::endTag('ul');

    Modal::end();
}