<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 22:56
 *
 * @var $mTermin Termin
 */

use app\modules\terminy\models\Termin;
use yii\helpers\Html;

$this->title = Yii::$app->name . ' - Přehled termínů';
$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    'Termíny'
);
?>
<div class="h2-buttons">
    <h2>Přehled termínů</h2>
    <?php
    echo Html::a(
        'Nový',
        array('/terminy/admin/pridat'),
        array(
            'class' => 'btn btn-success'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?php
echo \yii\grid\GridView::widget([
    'dataProvider' => $mTermin->search(),
    'columns' => $mTermin->vratSloupce()
])
?>