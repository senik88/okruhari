<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 11. 12. 2015
 * Time: 17:40
 *
 * @var $mTermin Termin
 * @var $mPlatba PlatbaSearchForm
 * @var $aDetaily array
 */
use app\modules\terminy\models\Platba;
use app\modules\terminy\models\PlatbaSearchForm;
use app\modules\terminy\models\Termin;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = Yii::$app->name . ' - Přehled plateb k termínu';
$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    ['url' => ['/terminy/admin/index'], 'label' => 'Termíny'],
    'Platby'
);
?>

<div class="h2-buttons">
    <h2>Platby k termínu <?= Yii::$app->formatter->asDate($mTermin->cas_od) ?></h2>
    <?php
    echo Html::a(
        'Potvrdit přijetí platby',
        '#',
        array(
            'class' => 'btn btn-info',
            'data-toggle' => 'modal',
            'data-target' => '#platba-potvrdit-modal'
        )
    );
    ?>
    <div class="clearfix"></div>
</div>

<?php
$form = ActiveForm::begin(array(
    'id' => 'platby-search-form',
    'action' => array('/terminy/admin/platby', 'termin' => $mTermin->termin_pk),
    'method' => 'get',
//    'type' => 'horizontal',
    'options' => array(
        'class' => 'well'
    ),
    'enableClientValidation' => false
//    'formConfig' => ['labelSpan' => 3, 'spanSize' => ActiveForm::SIZE_SMALL]
));
?>

    <div class="row">
        <div class="col-md-6 text-center">
            <p>
                Očekávaných plateb: <?= $aDetaily['c_ocekavano'] ?> za celkem <?= Yii::$app->formatter->asCurrency($aDetaily['s_ocekavano']) ?>
            </p>
        </div>
        <div class="col-md-6 text-center">
            <p>
                Zaplacených plateb: <?= $aDetaily['c_zaplaceno'] ?> za celkem <?= Yii::$app->formatter->asCurrency($aDetaily['s_zaplaceno']) ?>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($mPlatba, 'kod') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($mPlatba, 'stav')->dropDownList(['' => 'Všechny'] + Platba::itemAlias('stavy'))->label('Stav platby') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($mPlatba, 'uzivatel_pk')->label('Přihlášený') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton('Filtrovat', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

<?php $form->end() ?>

<div class="row">
    <div class="col-md-12">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $mPlatba->vratPlatbyTerminu($mTermin->termin_pk),
            'columns' => [
                'kod' => [
                    'attribute' => 'kod',
                    'label' => $mPlatba->getAttributeLabel('kod')
                ],
                'castka' => [
                    'attribute' => 'castka',
                    'label' => $mPlatba->getAttributeLabel('castka'),
                    'value' => function ($data) {
                        return Yii::$app->formatter->asCurrency($data['castka']);
                    },
                ] ,
                'zaplaceno' => [
                    'attribute' => 'castka_zaplacena',
                    'label' => $mPlatba->getAttributeLabel('castka_zaplacena'),
                    'value' => function ($data) {
                        return Yii::$app->formatter->asCurrency($data['castka_zaplacena']);
                    },
                ],
                'kreditu' => [
                    'attribute' => 'kreditu',
                    'label' => $mPlatba->getAttributeLabel('kreditu'),
                    'value' => function ($data) {
                        return Yii::$app->formatter->asCurrency($data['kreditu']);
                    },
                ],
                'datum' => [
                    'attribute' => 'datum',
                    'label' => $mPlatba->getAttributeLabel('datum'),
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDatetime($data['datum']);
                    }
                ],
                'uzivatel_pk' => [
                    'label' => 'Přihlášený',
                    'value' => function ($data) {
                        return sprintf('%s %s (%s)', $data['prijmeni'], $data['jmeno'], $data['nick']);
                    }
                ],
                'stav' => [
                    'label' => $mPlatba->getAttributeLabel('stav'),
                    'value' => function ($data) {
                        return \app\modules\terminy\models\TerminPrihlaseni::itemAlias('stavy', $data['stav']);
                    }
                ],
                'cas_prihlaseni' => [
                    'attribute' => 'cas_prihlaseni',
                    'label' => 'Čas přihlášení',
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDatetime($data['cas_prihlaseni']);
                    }
                ],
                'akce' => [
                    'class' => 'app\components\columns\ActionColumn',
                    'template' => '{potvrdit}',
                    'buttons' => [
                        'potvrdit' => function ($url, $model, $index) {
                            /** @var Platba $mPlatba */
                            $mPlatba = Platba::findOne($model['platba_pk']);

                            if ($mPlatba->stav == Platba::STAV_OCEKAVANA) {
                                return Html::a(
                                    Html::icon('ok'),
                                    Url::to(['/terminy/platby/potvrdit', 'id' => $model['platba_pk']]),
                                    [
                                        'title' => 'Potvrdit přijetí platby'
                                    ]
                                );
                            } else {
                                return null;
                            }
                        }
                    ]
                ]
            ]
        ]) ?>
    </div>
</div>

<?php
\yii\bootstrap\Modal::begin([
    'id' => 'platba-potvrdit-modal',
    'header' => 'Přijetí platby'
]);

    $form = ActiveForm::begin([
        'action' => ['/terminy/platby/potvrdit'],
        'method' => 'POST',
        'id' => 'potvrdit-platbu-form'
    ]);
    ?>

    <?= $form->field($mPlatba, 'kod') ?>
    <?= $form->field($mPlatba, 'termin_pk')->hiddenInput()->label(false) ?>

    <?= Html::submitButton("Potvrdit", ['class' => 'btn btn-success']) ?>

    <?php $form->end(); ?>

<?php \yii\bootstrap\Modal::end();