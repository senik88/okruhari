<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 2. 6. 2015
 * Time: 19:55
 *
 * @var $mTermin Termin
 */

use app\modules\terminy\models\Termin;
use dosamigos\ckeditor\CKEditor;
use dosamigos\datetimepicker\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'termin-pridat-form',
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]);
?>

<div class="form-fields">
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($mTermin, 'datum')->widget(DateTimePicker::className(), [
                'template' => '{button}{input}{reset}',
                'pickButtonIcon' => 'glyphicon glyphicon-th',
                'clientOptions' => [
                    'language' => 'cs',
                    'weekStart' => 1,
                    'autoclose' => true,
                    'pickerPosition' => 'bottom-right',
                    'format' => 'd.m.yyyy',
                    'minView' => 2
                ]
            ])->label('Datum konání') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($mTermin, 'cas_od')->widget(DateTimePicker::className(), [
                'template' => '{button}{input}{reset}',
                'pickButtonIcon' => 'glyphicon glyphicon-time',
                'clientOptions' => [
                    'language' => 'cs',
                    'weekStart' => 1,
                    'autoclose' => true,
                    'pickerPosition' => 'bottom-right',
                    'format' => 'h:ii',
                    'startView' => 1,
                    'maxView' => 1
                ]
            ])->label('Čas ježdění od') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($mTermin, 'cas_do')->widget(DateTimePicker::className(), [
                'template' => '{button}{input}{reset}',
                'pickButtonIcon' => 'glyphicon glyphicon-time',
                'clientOptions' => [
                    'language' => 'cs',
                    'weekStart' => 1,
                    'autoclose' => true,
                    'pickerPosition' => 'bottom-right',
                    'format' => 'h:ii',
                    'startView' => 1,
                    'maxView' => 1
                ]
            ])->label('Čas ježdění do') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($mTermin, 'kapacita')->label('Kapacita termínu') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($mTermin, 'cena')->label('Cena termínu') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($mTermin, 'zverejnit')->widget(DateTimePicker::className(), [
                'template' => '{button}{input}{reset}',
                'pickButtonIcon' => 'glyphicon glyphicon-th',
                'clientOptions' => [
                    'language' => 'cs',
                    'weekStart' => 1,
                    'autoclose' => true,
                    'pickerPosition' => 'bottom-right',
                    'format' => 'd.m.yyyy h:ii',
                ]
            ])->label('Čas zveřejnění') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($mTermin, 'popis')->widget(\yii\redactor\widgets\Redactor::className(), [
                'clientOptions' => [
                    'minHeight' => 250
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($mTermin, 'soubory')->widget(\kartik\file\FileInput::className(), [
                'options' => [
                    'multiple' => true,
                    'name' => 'Termin[soubory][]',
                ],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
            ])->label('Přiložit soubory') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($mTermin, 'prihlasitSpravce')->checkbox()->label('Přihlásit mě jako účastníka') ?>
        </div>
    </div>
</div>

<div class="form-actions well">
    <?= Html::submitButton('Uložit', [
        'class' => 'btn btn-success'
    ]) ?>
</div>

<?php
ActiveForm::end();

// todo na timepickery naveset JS
// http://www.malot.fr/bootstrap-datetimepicker/index.php