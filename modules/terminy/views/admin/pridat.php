<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 2. 6. 2015
 * Time: 19:55
 *
 * @var $this View
 * @var $mTermin Termin
 */

use app\modules\terminy\models\Termin;
use yii\web\View;

$this->title = Yii::$app->name . ' - Nový termín';
$this->params['breadcrumbs'] = array(
    ['label' => 'Administrace', 'url' => ['/admin/default/index']],
    ['url' => ['/terminy/admin/index'], 'label' => 'Termíny'],
    'Nový'
);
?>
<div class="h2-buttons">
    <h2>Nový termín</h2>
    <div class="clearfix"></div>
</div>

<?php
echo $this->render('_form', ['mTermin' => $mTermin]);
