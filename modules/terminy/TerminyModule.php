<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 31. 5. 2015
 * Time: 16:19
 */

namespace app\modules\terminy;


use yii\base\Module;

class TerminyModule extends Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\terminy\controllers';

    /**
     * @var
     */
    public $name = "Termíny";
}