<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 16. 6. 2015
 * Time: 18:54
 */

namespace app\modules\terminy\models;


class Pocasi
{

    /**
     * @var string
     */
    public $zdroj = 'openweathermap';

    /**
     * Jedna z techto moznosti: json, xml, html, text
     * @var string
     */
    public $typ = 'json';

    /**
     * @param $pk
     * @return array
     */
    public function vratProTermin($pk)
    {
        $mTermin = (new Termin())->nactiPodlePk($pk);

        $date1 = new \DateTime(date('Y-m-d', time()));
        $date2 = new \DateTime(date('Y-m-d', strtotime($mTermin->cas_od)));

        $diff = $date1->diff($date2);

//        if ($diff->days > 16) {
//            return [];
//        } else {
            return $this->nacti($pk, $diff->days);
//        }
    }

    /**
     *
     */
    protected function settings()
    {
        $settings['url'] = 'http://api.openweathermap.org/data/2.5/forecast/daily?q={$mesto}&mode={$typ}&cnt={$dni}&units=metric&APPID=b9fbdc9212340e73d7a52fb9496aa8f0';
        $settings['ico'] = 'http://openweathermap.org/img/w/{$ico}.png';

        return $settings;
    }

    /**
     * @param $pk
     * @param $dni
     * @return array
     */
    protected function nacti($pk, $dni)
    {
        $set = $this->settings();

        $url = strtr($set['url'], [
            '{$mesto}' => urlencode('Vysoké Mýto'),
            '{$typ}' => $this->typ,
            '{$dni}' => $dni
        ]);

        $filename = "termin_" . str_pad($pk, 6, '0', STR_PAD_LEFT);
        $path = \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'predpovedi';
        $fullpath = $path . DIRECTORY_SEPARATOR . $filename;

        if (!file_exists($path)) {
            mkdir($path);
        }

        if (!file_exists($fullpath)) {
            $json = file_get_contents($url);
            file_put_contents($fullpath, $json);
        } else {
            $file_time = filemtime($fullpath);

            $older_than = 1800; // 60s x 30m

            if ($file_time < time() - $older_than) {
                $json = file_get_contents($url);
                file_put_contents($fullpath, $json);
            }
        }

        $data = json_decode(file_get_contents($fullpath), true);

        $predpoved = end($data['list']);

        $result = [
            $predpoved,
            strtr($set['ico'], ['{$ico}' => $predpoved['weather'][0]['icon']])
        ];

        return $result;
    }
}