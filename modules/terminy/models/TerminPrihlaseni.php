<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 2. 6. 2015
 * Time: 19:23
 */

namespace app\modules\terminy\models;


use app\components\ItemAliasTrait;
use app\mail\models\AutomatickePrihlaseniMail;
use app\modules\uzivatel\models\Uzivatel;
use yii\db\ActiveRecord;

/**
 * Class TerminPrihlaseni
 * @package app\modules\terminy\models
 *
 * @property int termin_prihlaseni_pk
 * @property int termin_pk
 * @property int uzivatel_pk
 * @property int platba_pk
 * @property string stav
 * @property string cas_prihlaseni
 */
class TerminPrihlaseni extends ActiveRecord
{
    use ItemAliasTrait;

    const STAV_NAHRADNIK = 'NAHRADNIK';
    const STAV_PRIHLASENY = 'PRIHLASENY';
    const STAV_POTVRZENY = 'POTVRZENY';
    const STAV_ZRUSENY = 'ZRUSENY';

    /**
     * @var Uzivatel
     */
    public $uzivatel;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * todo nevim, jestli na to není nějaký systémový řešení
     *
     * @param array $row
     * @return TerminPrihlaseni
     */
    public static function instancuj($row)
    {
        $class = new self;
        $class->load($row, '');

        return $class;
    }

    /**
     * Hash je zatim jen base64 encoded termin_pk a uzivatel_pk.<br>
     * Casem bude jiny zpusob, az vymyslim lepsi.
     *
     * @param $hash
     * @return $this
     */
    public function naplnPodleHashe($hash)
    {
        $decoded = self::prectiHash($hash);

        $this->termin_pk = $decoded['termin'];
        $this->uzivatel_pk = $decoded['uzivatel'];

        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function prihlas()
    {
        $this->_overTerminUzivatele();

        $sql = 'SELECT * FROM uzivatel_prihlas_termin(:termin, :uzivatel)';
        $params = [
            ':termin' => $this->termin_pk,
            ':uzivatel' => $this->uzivatel_pk
        ];

        $command = \Yii::$app->db->createCommand($sql)->bindValues($params);
        $result = $command->queryOne();

        if ($result['r_prihlaseni_pk'] > 0) {
            return true;
        } else {
            // zalogovat
            throw new \Exception($result['r_popis']);
        }
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function odhlas()
    {
        $db = \Yii::$app->db;
        $this->_overTerminUzivatele();

        $sql = 'select uzivatel_odhlas_termin(:termin, :uzivatel)';
        $params = [
            'termin' => $this->termin_pk,
            'uzivatel' => $this->uzivatel_pk
        ];

        $trans = $db->beginTransaction();
        try {
            $command = $db->createCommand($sql)->bindValues($params);
            $result = $command->queryScalar();

            if ($result > 0) {
                /** @var TerminPrihlaseni $novePrihlaseni */
                $novePrihlaseni = TerminPrihlaseni::findOne($result);
                /** @var Uzivatel $mUzivatel */
                $mUzivatel = Uzivatel::findOne($novePrihlaseni->uzivatel_pk);

                $novePrihlaseniMail = new AutomatickePrihlaseniMail();
                $novePrihlaseniMail->adresat = $mUzivatel->email;
                $novePrihlaseniMail->params = [
                    'termin_pk' => $novePrihlaseni->termin_pk,
                ];

                $novePrihlaseniMail->send();

                $return = true;
            } else if ($result === 0) {
                $return = true;
            } else {
                // zalogovat
                $return = false;
            }

            $trans->commit();
            return $return;
        } catch (\Exception $e) {
            $trans->rollBack();
            return false;
        }
    }

    /**
     * Vse opet provadet nad databazi
     *
     * @param float|null $kreditu
     * @return bool
     */
    public function uplatniKredity($kreditu = null)
    {
        $sql = "SELECT * FROM termin_uplatni_kredity(:pk, :kreditu)";
        $params = [
            ':pk' => $this->termin_prihlaseni_pk,
            ':kreditu' => $kreditu
        ];

        $result = \Yii::$app->db->createCommand($sql, $params)->queryOne();

        if ($result['r_vysledek'] == 'OK') {
            return $result['r_platba_pk'];
        } else {
            \Yii::error("Nepodarilo se uplatnit kredity, chyba: {$result['r_vysledek']}");
            return false;
        }
    }

    /**
     * @param $termin
     * @param $uzivatel
     * @return string
     */
    public static function sestavHash($termin, $uzivatel)
    {
        $hash = base64_encode($termin . '#' . $uzivatel);

        return $hash;
    }

    /**
     * @param $hash
     * @return array
     * @throws \Exception
     */
    public static function prectiHash($hash)
    {
        $decoded = base64_decode($hash);

        if (!preg_match('#^(?P<termin>\d+)\#(?P<uzivatel>\d+)$#', $decoded, $matches)) {
            throw new \Exception("Neplatny hash pro prihlaseni na termin!");
        }

        $return = [];
        $params = ['termin', 'uzivatel'];

        foreach ($params as $param) {
            $return[$param] = $matches[$param];
        }

        return $return;
    }

    /**
     * Číselníky pro:
     * - stavy přihlášení
     *
     * @return array
     */
    public static function itemAliasData()
    {
        return [
            'stavy' => [
                self::STAV_POTVRZENY => 'Potvrzený',
                self::STAV_PRIHLASENY => 'Přihlášený',
                self::STAV_NAHRADNIK => 'Náhradník',
                self::STAV_ZRUSENY => 'Zrušený'
            ]
        ];
    }

    protected function _overTerminUzivatele()
    {

    }
}