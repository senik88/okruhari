<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.12.2015
 * Time: 23:54
 */

namespace app\modules\terminy\models;


use app\components\SqlDataProvider;
use yii\base\Model;

/**
 * Class PlatbaSearchForm
 *
 * Trida pro vyhledavani plateb. Vsechny metody pro hledani presunuty sem, aby nebyl zapraseny puvodni model Platba.
 *
 * @package app\modules\admin\models
 */
class PlatbaSearchForm extends Model
{
    const SCENARIO_SEARCH = 'search';
    const SCENARIO_SUBMIT = 'submit';

    /**
     * @var integer variabilni symbol a kod pro prihlaseni
     */
    public $kod;

    /**
     * @var string stav platby
     */
    public $stav;

    /**
     * @var integer kdo ma platbu zaplatit
     */
    public $uzivatel_pk;

    /**
     * @var integer ke kteremu terminu se platba vaze
     */
    public $termin_pk;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SEARCH] = [
            'kod', 'stav', 'uzivatel_pk'
        ];
        $scenarios[self::SCENARIO_SUBMIT] = [
            'kod', 'termin_pk'
        ];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->scenario == self::SCENARIO_SUBMIT) {
            return [
                [['kod'], 'required']
            ];
        } else {
            return [
                [['kod', 'stav', 'uzivatel_pk'], 'safe']
            ];
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kod' => 'Kód',
            'castka' => 'Částka',
            'castka_zaplacena' => 'Zaplaceno',
            'kreditu' => 'Kreditů',
            'datum' => 'Datum',
            'stav' => 'Stav'
        ];
    }

    /**
     * @param $termin
     * @return SqlDataProvider
     */
    public function vratPlatbyTerminu($termin)
    {
        $sql = "
            SELECT
                p.*
                , tp.*
                , u.jmeno
                , u.prijmeni
                , u.nick
                , coalesce(p.castka_zaplacena, 0.00) as castka_zaplacena
                , coalesce(sub.kreditu, 0.00) AS kreditu
            FROM platba p
                JOIN termin_prihlaseni tp ON tp.platba_pk = p.platba_pk
                JOIN uzivatel u ON tp.uzivatel_pk = u.uzivatel_pk
                LEFT JOIN (
                    SELECT
                        platba_pk
                        , sum(hodnota) as kreditu
                        , popis
                    FROM historie_uctu GROUP BY platba_pk, popis
                ) AS sub ON sub.platba_pk = p.platba_pk AND sub.popis = 'Uplatneni kreditu'
            WHERE tp.termin_pk = :pk
        ";

        $params = [
            ':pk' => $termin
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params
        ]);
    }

    /**
     * @param $termin
     * @return array|bool
     */
    public function nactiOcekavaneZaplaceneTerminu($termin)
    {
        $sql = "
            SELECT
                  count(*) AS c_ocekavano
                , coalesce(sum(zaplaceno), 0) AS c_zaplaceno
                , coalesce(sum(castka), 0.00) AS s_ocekavano
                , coalesce(sum(castka_zaplacena), 0.00) AS s_zaplaceno
            FROM (
                SELECT
                    p.castka
                    , coalesce(p.castka_zaplacena, 0.00) AS castka_zaplacena
                    , CASE WHEN p.stav = 'ZAPLACENA' THEN 1 ELSE 0 END AS zaplaceno
                FROM termin_prihlaseni tp
                    LEFT JOIN platba p ON tp.platba_pk = p.platba_pk
                WHERE tp.termin_pk = :pk
                    AND p.stav IN ('OCEKAVANA', 'ZAPLACENA')
            ) AS T;
        ";

        $params = [
            ':pk' => $termin
        ];

        return \Yii::$app->db->createCommand($sql, $params)->queryOne();
    }
}