<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 4. 6. 2015
 * Time: 19:56
 */

namespace app\modules\terminy\models;


use app\components\ItemAliasTrait;
use app\components\SqlDataProvider;
use app\mail\models\PotvrzeniPrihlaseniMail;
use app\modules\uzivatel\models\Uzivatel;
use yii\db\ActiveRecord;

/**
 * Class Platba
 * @package app\modules\terminy\models
 *
 * @property int platba_pk
 * @property float castka
 * @property float castka_zaplacena
 * @property string datum
 * @property float refundace
 * @property string stav
 * @property int kod
 */
class Platba extends ActiveRecord
{
    use ItemAliasTrait;

    const STAV_OCEKAVANA    = 'OCEKAVANA';
    const STAV_ZAPLACENA    = 'ZAPLACENA';
    const STAV_ZRUSENA      = 'ZRUSENA';
    const STAV_REFUNDACE    = 'REFUNDACE'; // = vracena
    const STAV_PREVEDENA    = 'PREVEDENA';

    /**
     * @var
     */
    public $uzivatel_pk;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['platba_pk', 'castka', 'castka_zaplacena', 'datum', 'refundace', 'stav', 'kod'], 'safe'],
            [['kod'], 'integer', 'min' => 1000000, 'max' => 999999999]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'castka' => 'Částka',
            'castka_zaplacena' => 'Zaplaceno',
            'datum' => 'Čas zaplacení',
            'stav' => 'Stav',
            'kod' => 'Kód'
        ];
    }

    public function search() {

    }

    /**
     * @return SqlDataProvider
     */
    public function vratMojePlatby()
    {
        $sql = "
            SELECT
                p.*
                , tp.termin_prihlaseni_pk
                , tp.termin_pk
                , tp.stav as stav_prihlaseni
                , EXTRACT(EPOCH FROM t.cas_od) AS termin_datum_u
                , CASE WHEN p.stav = 'OCEKAVANA' AND u.ucet_kredity > 0::NUMERIC THEN 1 ELSE 0 END AS lze_pouzit_kredity
            FROM platba p
                JOIN termin_prihlaseni tp ON p.platba_pk = tp.platba_pk
                JOIN termin t ON tp.termin_pk = t.termin_pk
                JOIN uzivatel u ON u.uzivatel_pk = tp.uzivatel_pk
            WHERE tp.uzivatel_pk = :uzivatel
        ";

        $params = [
            'uzivatel' => $this->uzivatel_pk
        ];

        return new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'sort' => [
                'attributes' => ['platba_pk', 'cas_od', 'cas_do'],
                'defaultOrder' => ['cas_od' => SORT_DESC]
            ]
        ]);
    }

    /**
     * Nastavim stav platby na ZAPLACENA, prihlaseni na POTVRZENY a musim odeslat uzivateli email o prijeti platby a potvrzeni terminu
     */
    public function potvrdPrijeti()
    {
        $db = \Yii::$app->db;

        $sql = "SELECT * FROM platba_potvrdit_prijeti(:pk, :castka)";

        $params = [
            ':pk' => (int) $this->platba_pk,
            ':castka' => (float) $this->castka
        ];

        $trans = $db->beginTransaction();

        try {
            // tohle nastavi nove stavy na platbu a prihlaseni uzivatele
            // vraci mi to pk terminu prihlaseni
            $result = $db->createCommand($sql, $params)->queryOne();

            if ($result['r_platba_pk'] < 0) {
                throw new \Exception("nepodarilo se potvrdit platbu, vysledek: {$result['r_platba_pk']}");
            }

            /** @var TerminPrihlaseni $mTerminPrihlaseni */
            $mTerminPrihlaseni = TerminPrihlaseni::findOne($result['r_termin_prihlaseni']);
            /** @var Uzivatel $mUzivatel */
            $mUzivatel = Uzivatel::findOne($mTerminPrihlaseni->uzivatel_pk);

            $mail = new PotvrzeniPrihlaseniMail();
            $mail->adresat = $mUzivatel->email;
            $mail->params = [
                'termin_prihlaseni_pk' => $mTerminPrihlaseni->termin_prihlaseni_pk
            ];

            if (!$mail->send()) {
                throw new \Exception("nepodarilo se odeslat email s potvrzenim platby, termin_prihlaseni_pk = {$mTerminPrihlaseni->termin_prihlaseni_pk}");
            }

            $trans->commit();

            return true;
        } catch (\Exception $e) {
            \Yii::error("chyba pri potvrzovani platby: {$e->getMessage()}");
            $trans->rollBack();

            return false;
        }
    }

    /**
     * Číselníky pro:
     * - stavy plateb
     *
     * @return array
     */
    protected static function itemAliasData()
    {
        return [
            'stavy' => [
                self::STAV_OCEKAVANA => 'Očekávaná',
                self::STAV_PREVEDENA => 'Převedená',
                self::STAV_REFUNDACE => 'Refundace',
                self::STAV_ZAPLACENA => 'Zaplacená',
                self::STAV_ZRUSENA => 'Zrušená',
            ]
        ];
    }
}