<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 22:54
 */

namespace app\modules\terminy\models;


use app\components\helpers\DbUtils;
use app\components\ItemAliasTrait;
use app\components\SqlDataProvider;
use app\mail\models\NovyTerminMail;
use app\modules\admin\models\Soubor;
use app\modules\uzivatel\models\Uzivatel;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\web\UploadedFile;

/**
 * Class Termin
 * @package app\modules\terminy\models
 */
class Termin extends Model
{
    use ItemAliasTrait;

    const STAV_NOVY = 'NOVY';
    const STAV_VEREJNY = 'VEREJNY';
    const STAV_STORNO = 'STORNO';
    const STAV_ZRUSENY = 'ZRUSENY';

    const SCENARIO_PRIDAT = 'pridat';

    /**
     * @var
     */
    public $termin_pk;

    /**
     * @var
     */
    public $datum;

    /**
     * @var
     */
    public $cas_od;

    /**
     * @var
     */
    public $cas_do;

    /**
     * @var
     */
    public $stav;

    /**
     * @var
     */
    public $popis;

    /**
     * @var
     */
    public $kapacita;

    /**
     * @var
     */
    public $obsazeno;

    /**
     * @var TerminPrihlaseni[]
     */
    public $prihlaseni;

    /**
     * @var TerminPrihlaseni[]
     */
    public $nahradnici;

    /**
     * @var Soubor[]
     */
    public $soubory;

    /**
     * @var Uzivatel
     */
    public $vypsal;

    /**
     * @var
     */
    public $prihlasitSpravce = 1;

    /**
     * @var
     */
    public $zverejnit;

    /**
     * @var
     */
    public $cena;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $aScenarios = parent::scenarios();
        $aScenarios['pridat'] = array(
            'datum', 'cas_od', 'cas_do', 'kapacita', 'popis', 'zverejnit', 'prihlasitSpravce', 'cena'
        );

        return $aScenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        switch ($this->scenario) {
            case self::SCENARIO_PRIDAT:
                $rules = [
                    [['datum', 'cas_od', 'cas_do', 'kapacita', 'popis', 'cena'], 'required'],
                    [['zverejnit', 'prihlasitSpravce'], 'safe']
                ];
                break;
            case self::SCENARIO_DEFAULT:
                $rules = [
                    [$this->attributes(), 'safe']
                ];
                break;
            default:
                throw new \Exception("nezname scenario ({$this->scenario})");
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * @return SqlDataProvider
     */
    public function search()
    {
        return $this->_vratDataProvider();
    }

    /**
     * Metoda nacte veskere informace tykajici se terminu:
     * - detail terminu
     * - uzivatele, ktery termin vlozil
     * - prihlasene uzivatele
     * - nahradniky
     *
     * @param $pk
     * @return $this
     */
    public function nactiPodlePk($pk)
    {
        $db = \Yii::$app->db;

        $detailSQL = 'select * from termin where termin_pk = :pk';
        $detail = $db->createCommand($detailSQL)->bindValue('pk', $pk)->queryOne();

        // pokud jsem nenasel data, tak vratim null
        if (false === $detail) {
            return null;
        }

        // nahraju data vytazena z databaze
        $this->load($detail, '');

        // nahraju uzivatele, ktery vytvoril termin
        $this->vypsal = Uzivatel::findOne(['uzivatel_pk' => $detail['uzivatel_pk']]);

        // nahraju si do terminu prihlasene ucastniky
        $prihlaseniSQL = '
          select
            * ,
            case when stav = :stav1 then 1 else 2 end as razeni_stav
          from termin_prihlaseni
          where termin_pk = :pk and stav in (:stav1, :stav2)
          order by razeni_stav asc, cas_prihlaseni asc
        ';

        $command = $db->createCommand($prihlaseniSQL)->bindValues([
            ':pk' => $pk,
            ':stav1' => TerminPrihlaseni::STAV_PRIHLASENY,
            ':stav2' => TerminPrihlaseni::STAV_POTVRZENY,
        ]);

        foreach ($command->queryAll() as $row) {
            /** @var TerminPrihlaseni $model */
            $model = TerminPrihlaseni::findOne($row['termin_prihlaseni_pk']);
            $model->uzivatel = Uzivatel::findOne($model->uzivatel_pk);

            $this->prihlaseni[] = $model;
        }

        // nahraju si do terminu nahradniky
        $nahradniciSQL = 'select * from termin_prihlaseni where termin_pk = :pk and stav = :stav1 order by cas_prihlaseni asc';
        $command = $db->createCommand($nahradniciSQL)->bindValues([
            'pk' => $pk,
            'stav1' => TerminPrihlaseni::STAV_NAHRADNIK
        ]);

        foreach ($command->queryAll() as $row) {
            /** @var TerminPrihlaseni $model */
            $model = TerminPrihlaseni::findOne($row['termin_prihlaseni_pk']);
            $model->uzivatel = Uzivatel::findOne($model->uzivatel_pk);

            $this->nahradnici[] = $model;
        }

        $souborySQL = "select * from termin_soubor ts JOIN soubor s ON ts.soubor_pk = s.soubor_pk WHERE ts.termin_pk = :pk";
        $command = $db->createCommand($souborySQL, [
            ':pk' => $this->termin_pk
        ]);

        foreach ($command->queryAll() as $row) {
            $mSoubor = new Soubor();

            if ($mSoubor->load($row, '')) {
                $this->soubory[] = $mSoubor;
            }
        }

        return $this;
    }

    /**
     * Metoda zavola proceduru termin_zmena
     *
     *
     */
    public function uloz()
    {
        $db = \Yii::$app->db;
        $transakce = $db->beginTransaction();

        $sql = 'select termin_zmena(:pk, :cas_od, :cas_do, :zverejnit, :stav, :popis, :kapacita, :vypsal, :cena)';
        try {
            $params = [
                'pk' => $this->termin_pk,
                'cas_od' => date('Y-m-d H:i:s', strtotime($this->datum . ' ' . $this->cas_od)),
                'cas_do' => date('Y-m-d H:i:s', strtotime($this->datum . ' ' . $this->cas_do)),
                'zverejnit' => date('Y-m-d H:i:s', (null != $this->zverejnit ? strtotime($this->zverejnit) : time())),
                'stav' => (null != $this->zverejnit ? self::STAV_NOVY : self::STAV_VEREJNY),
                'popis' => $this->popis,
                'kapacita' => $this->kapacita,
                'vypsal' => \Yii::$app->user->id,
                'cena' => $this->cena
            ];

            $termin_pk = $db->createCommand($sql)->bindValues($params)->queryScalar();

            /** @var UploadedFile $soubor */
            foreach ($this->soubory as $soubor) {
                $mSoubor = new Soubor();
                $mSoubor->nazev = $soubor->name;
                $mSoubor->typ = $soubor->type;
                $mSoubor->velikost = $soubor->size;
                $mSoubor->hash = md5(file_get_contents($soubor->tempName));

                if ($mSoubor->uloz()) {
                    $path = \Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'upload';

                    if (!file_exists($path)) {
                        mkdir($path);
                    }

                    $full_path = $path . DIRECTORY_SEPARATOR . $mSoubor->soubor_pk;

                    $soubor->saveAs($full_path);

                    $mSoubor->priradTerminu($termin_pk);
                } else {
                    ddd("nepodarilo se ulozit soubor");
                }
            }

            // naplanuje rozeslani na datum zverejneni
            $this->informujNovyTermin($termin_pk);

            $transakce->commit();

            return $termin_pk;
        } catch (\Exception $e) {
            $transakce->rollBack();
            throw $e;
        }
    }

    /**
     * Metoda rozesle registrovanym uzivatelum informace o novem terminu
     * @param $termin_pk
     * @throws \Exception
     */
    public function informujNovyTermin($termin_pk)
    {
        $db = \Yii::$app->db;
        $sql = 'select u.* from uzivatel u join uzivatel_odber uo on u.uzivatel_pk = uo.uzivatel_pk where uo.terminy = true and u.stav = :stav';

        $data = $db->createCommand($sql)->bindValue('stav', Uzivatel::STAV_AKTIVNI)->queryAll();

        // try catch, pridat sloupec chyba, instancovat jednou, pak unset attributes
        foreach ($data as $row) {
            $mail = new NovyTerminMail();
            $mail->adresat = $row['email'];
            $mail->params = [
                'termin_pk' => $termin_pk,
                'uzivatel_pk' => $row['uzivatel_pk']
            ];

            if (null != $this->zverejnit) {
                $mail->cas_naplanovani = $this->zverejnit;
            }

            $mail->send();
        }
    }

    /**
     * Vraci instanci nejblizsiho terminu
     */
    public function vratNejblizsi()
    {
        $sql = "
            SELECT
                t.*
                , count(tp.termin_prihlaseni_pk) as obsazeno
            FROM termin t
                LEFT JOIN termin_prihlaseni tp ON t.termin_pk = tp.termin_pk AND tp.stav IN ('PRIHLASENY', 'POTVRZENY')
            WHERE cas_od > now()
            GROUP BY t.termin_pk
            ORDER by cas_od ASC LIMIT 1
        ";

        $data = \Yii::$app->db->createCommand($sql)->queryOne();

        if (!$this->load($data, '')) {
            return null;
        }

        return $this;
    }

    /**
     *
     */
    public function vratSloupce() {
        return [
            'datum' => [
                'attribute' => 'datum',
                'value' => function ($data) {
                    return date('j.n.Y', strtotime($data['cas_od']));
                }
            ],
            'cas' => [
                'label' => 'Čas',
                'value' => function ($data) {
                    return sprintf(
                        '%s až %s',
                        date('H:i', strtotime($data['cas_od'])),
                        date('H:i', strtotime($data['cas_do']))
                    );
                }
            ],
            'stav' => [
                'attribute' => 'stav'
            ],
            'kapacita' => [
                'attribute' => 'kapacita',
                'label' => 'Volno',
                'value' => function ($data) {
                    return sprintf('%d / %d', $data['volno'], $data['kapacita']);
                }
            ],
            'cena' => [
                'attribute' => 'cena',
                'class' => 'app\components\columns\PriceColumn',
                'mena' => 'Kč'
            ],
            'vlozil' => [
                'attribute' => 'vlozil'
            ],
            'akce' => [
                'class' => 'app\components\columns\ActionColumn',
                'template' => '{view} {platby} {zrusit}',
                'buttons' => [
                    'platby' => function ($url) {
                        return Html::a(
                            Html::icon('piggy-bank'),
                            $url,
                            [
                                'title' => 'Přehled plateb termínu'
                            ]
                        );
                    },
                    'zrusit' => function ($url) {
                        return Html::a(
                            Html::icon('remove'),
                            $url,
                            [
                                'title' => 'Zrušit termín'
                            ]
                        );
                    }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    switch ($action) {
                        case 'view':
                            $url = ['/terminy/default/detail', 'id' => $model['termin_pk']];
                            break;
                        case 'platby':
                            $url = ['/terminy/admin/platby', 'termin' => $model['termin_pk']];
                            break;
                        case 'zrusit':
                            $url = ['/terminy/admin/zrusit', 'termin' => $model['termin_pk']];
                            break;
                        default:
                            throw new \Exception("Undefined action for model: $action");
                    }
                    return $url;
                },
            ]
        ];
    }

    /**
     * @param int|bool $pagination
     * @return SqlDataProvider
     */
    protected function _vratDataProvider($pagination = 10)
    {
        $sql = "
          select
              t.*
            , u.prijmeni || ' ' || u.jmeno as vlozil
            , (t.kapacita - (select count(*) from termin_prihlaseni where stav in ('PRIHLASENY', 'POTVRZENY') and termin_pk = t.termin_pk)) as volno
            , CASE WHEN t.cas_od < now() THEN 1 ELSE 0 END AS probehly
          from termin t
            join uzivatel u on t.uzivatel_pk = u.uzivatel_pk
        ";

        $params = [];
        $where = [];

        if (null != $this->zverejnit) {
            $where[] = "t.zverejnit < :zverejnit";
            $params[':zverejnit'] = date('Y-m-d H:i:s', $this->zverejnit);
        }

        if (null != $this->stav) {
            if (!is_array($this->stav)) {
                $this->stav = [$this->stav];
            }

            $where[] = "t.stav = ANY(:stavy)";
            $params[':stavy'] = DbUtils::implodePgArray($this->stav);
        }

        if (!empty($where)) {
            $sql .= " WHERE " . implode(" AND ", $where);
        }

        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'pagination' => !is_int($pagination) ? false : [
                'pageSize' => $pagination
            ],
            'sort' => [
                'defaultOrder' => ['cas_od' => SORT_DESC],
                'attributes' => [
                    'cas_od'
                ]
            ],
        ]);

        return $provider;
    }
}