<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 12. 12. 2015
 * Time: 13:43
 *
 * @var $klic string
 */

$url = Yii::$app->urlManager->createAbsoluteUrl(['/uzivatel/default/aktivace', 'klic' => $klic]);
?>

<h2>Děkujeme za registraci</h2>

<p>
    V aplikaci jsou vyvěšovány termíny pro ježdění ve Vysokém Mýtě.
</p>

<p>
    Pokud jste se přihlásil(a) k odběru informací o termínech, budete informován(a), jakmile bude vypsán nový termín.
    <br>
    Na termín je možné se přihlásit v přehledu termínů nebo v detailu vypsaného.
</p>

<p>
    Pro aktivaci účtu použijte tento odkaz:
</p>

<p class="well text-center">
    <a href="<?= $url ?>"><?= $url ?></a>
</p>