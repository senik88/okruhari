<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 13. 12. 2015
 * Time: 17:24
 *
 * @var $termin_pk integer
 * @var $mTermin Termin
 * @var $detailUrl string
 */
use app\modules\terminy\models\Termin;

?>

<h2>Byl vypsán nový termín</h2>

<p>
    Můžete se přihlásit na nový termín ježdění ve Vysokém Mýtě. Detaily termínu:
</p>

<ul>
    <li>Začátek: <?= Yii::$app->formatter->asDatetime($mTermin->cas_od) ?></li>
    <li>Konec: <?= Yii::$app->formatter->asDatetime($mTermin->cas_do) ?></li>
    <li>Cena: <?= Yii::$app->formatter->asCurrency($mTermin->cena) ?></li>
</ul>

<p>
    Na termín se můžete přihlásit přes detail termínu na: <a href="<?= $detailUrl ?>"><?= $detailUrl ?></a>.
</p>