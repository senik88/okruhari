<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.12.2015
 * Time: 22:44
 *
 * @var Platba $mPlatba
 * @var TerminPrihlaseni $mTerminPrihlaseni
 * @var Termin $mTermin
 *
 * @var string $url
 */
use app\modules\terminy\models\Platba;
use app\modules\terminy\models\Termin;
use app\modules\terminy\models\TerminPrihlaseni;
?>

<h2>Potvrzujeme přijetí platby a přihlášení</h2>

<ul>
    <li>Místo: Vysoké Mýto</li>
    <li>Datum: <?= Yii::$app->formatter->asDate($mTermin->cas_od) ?></li>
    <li>Čas od: <?= Yii::$app->formatter->asTime($mTermin->cas_od, 'php:H:i') ?></li>
    <li>Čas do: <?= Yii::$app->formatter->asTime($mTermin->cas_do, 'php:H:i') ?></li>
</ul>

<p>
    Podrobnosti platby:
</p>

<ul>
    <li>Variabilní symbol: <?= $mPlatba->kod ?></li>
    <li>Očekávaná částka: <?= Yii::$app->formatter->asCurrency($mPlatba->castka) ?></li>
    <li>Zaplacená částka: <?= Yii::$app->formatter->asCurrency($mPlatba->castka_zaplacena) ?></li>
    <li>Stav: <?= Platba::itemAlias('stavy', $mPlatba->stav) ?></li>
</ul>

<p>
    Detail termínu: <a href="<?= $url ?>"><?= $url ?></a>
</p>