<?php
/**
 * Created by PhpStorm.
 * User: repad
 * Date: 2.12.14
 * Time: 13:04
 */

namespace app\mail;


use Exception;
use yii\mail\BaseMailer;
use yii\mail\MessageInterface;
use Yii;

/**
 * Class Mailer
 * @package app\mail
 */
class Mailer extends BaseMailer
{
    /**
     * @var
     */
    public $subaccount_id;

    /**
     * @var
     */
    public $ga;

    /**
     * @var
     */
    public $preserveRecipients;

    /**
     * @var string
     */
    public $messageClass = 'app\mail\Message';

    /**
     * @var array
     */
    protected $_aDevTo;

    /**
     * @var array
     */
    protected $_aDevCc;

    /**
     * @var array
     */
    protected $_aDevBcc;

    /**
     * @var string chybova hlaska odchycena pri odesilani emailu
     */
    protected $_errorInfo;

    /**
     * @var
     */
    protected $_apiKey;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $mailerParams = Yii::$app->params['mail'];
        $this->_apiKey = $mailerParams['apiKey'];

        if (isset($mailerParams['development'])) {
            $aDevelopment = $mailerParams['development'];

            if (isset($aDevelopment['devMailTo'])) $this->_aDevTo = $aDevelopment['devMailTo'];
            if (isset($aDevelopment['devMailCc'])) $this->_aDevCc = $aDevelopment['devMailCc'];
            if (isset($aDevelopment['devMailBcc'])) $this->_aDevBcc = $aDevelopment['devMailBcc'];
        }
    }

    /**
     *
     */
    public function init()
    {
        parent::init();
        $this->subaccount_id = Yii::$app->params['mail']['mandrill_key'];
        $this->ga = Yii::$app->params['mail']['google_analytics'];
        $this->preserveRecipients = false;
    }

    /**
     * Sends the specified message.
     * This method should be implemented by child classes with the actual email sending logic.
     * @param Message $message the message to be sent
     * @return boolean whether the message is sent successfully
     */
    protected function sendMessage($message)
    {
        // poskladam konfiguracni pole
        $a_mandrill_conf = array(
            'from' => $message->getFrom(),
            'email' => array(
                'subject' => $message->getSubject(),
                'text_body' => $message->textBody,
                'html_body' => $message->isHtml() ? $message->htmlBody : null,
                'attachments' => $message->attachments,
                'images' => $message->images,
                'global_merge_vars' => [],
                'merge_vars' => [],
            ),

            'to' => array_merge($message->getTo(), $message->getCc(), $message->getBcc()),

            'subaccount_id' => $this->subaccount_id,

            'preserve_recipients' => $this->preserveRecipients,

            'ga' => $this->ga,
        );

        // odeslu
        try {
            $oSender = new \Mandrill();
            $result = $oSender->messages->send($a_mandrill_conf);
        } catch (Exception $e) {
            $this->_errorInfo = $e->getMessage();
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Pokud jsem na testu, zahodi vsechny prijemce a nastavi vyvojarske z konfigu
     * @param Message $message
     * @return bool
     */
    public function beforeSend($message)
    {
        if (parent::beforeSend($message)) {
            if (YII_DEBUG) {
                $message->clearAllRecipients();

                if (null != $this->_aDevTo) {
                    $message->setTo($this->_aDevTo);
                }

                if (null != $this->_aDevCc) {
                    $message->setCc($this->_aDevCc);
                }

                if (null != $this->_aDevBcc) {
                    $message->setBcc($this->_aDevBcc);
                }
            }

            return true;
        } else {
            $this->_errorInfo = 'chyba v rodicovske metode beforeSend()';
            return false;
        }
    }

    /**
     * Vrati chybovou hlasku maileru
     *
     * @return mixed
     */
    public function getErrorInfo()
    {
        return $this->_errorInfo;
    }
}