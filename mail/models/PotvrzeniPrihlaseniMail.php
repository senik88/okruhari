<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 16.12.2015
 * Time: 16:22
 */

namespace app\mail\models;


use app\mail\Email;
use app\modules\terminy\models\Platba;
use app\modules\terminy\models\Termin;
use app\modules\terminy\models\TerminPrihlaseni;

class PotvrzeniPrihlaseniMail extends Email
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->predmet = "Potvrzení platby a přihlášení";
    }

    /**
     * metoda vezme zaznam z databaze, naplni sablonu daty a vrati vykreslene telo emailu jako string
     *
     * @throws \Exception
     * @return string
     */
    public function vykresliEmail()
    {
        /** @var TerminPrihlaseni $mTerminPrihlaseni */
        $mTerminPrihlaseni = TerminPrihlaseni::findOne($this->data['termin_prihlaseni_pk']);
        $mPlatba = Platba::findOne($mTerminPrihlaseni->platba_pk);
        $mTermin = (new Termin())->nactiPodlePk($mTerminPrihlaseni->termin_pk);

        $url = \Yii::$app->urlManager->createAbsoluteUrl(['/terminy/default/detail', 'id' => $mTermin->termin_pk]);

        /*ddd(
            $mTerminPrihlaseni->attributes,
            $mPlatba->attributes
        );*/

        return $this->_mailer->view->render('@app/mail/views/potvrzeni-prihlaseni', [
            'mTerminPrihlaseni' => $mTerminPrihlaseni,
            'mPlatba' => $mPlatba,
            'mTermin' => $mTermin,
            'url' => $url
        ]);
    }
}