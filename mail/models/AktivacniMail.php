<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 2. 6. 2015
 * Time: 17:33
 */

namespace app\mail\models;


use app\mail\Email;

/**
 * Class AktivacniMail
 * @package app\mail\models
 */
class AktivacniMail extends Email
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->predmet = "Aktivace účtu";
    }

    /**
     * metoda vezme zaznam z databaze, naplni sablonu daty a vrati vykreslene telo emailu jako string
     *
     * @throws \Exception
     * @return string
     */
    public function vykresliEmail()
    {
        return $this->_mailer->view->render('@app/mail/views/aktivace', ['klic' => $this->data['klic']]);
    }
}