<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 2. 6. 2015
 * Time: 21:06
 */

namespace app\mail\models;


use app\mail\Email;
use app\modules\terminy\models\Termin;
use app\modules\terminy\models\TerminPrihlaseni;
use yii\helpers\Url;

/**
 * Class NovyTerminMail
 * @package app\mail\models
 */
class NovyTerminMail extends Email
{
    /**
     * @var Termin
     */
    protected $mTermin;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->predmet = "Byl vypsán nový termín";

        if (isset($this->data['termin_pk'])) {
            $this->mTermin = (new Termin())->nactiPodlePk($this->data['termin_pk']);
        }
    }

    /**
     * metoda vezme zaznam z databaze, naplni sablonu daty a vrati vykreslene telo emailu jako string
     *
     * @throws \Exception
     * @return string
     */
    public function vykresliEmail()
    {
        $termin_pk = $this->data['termin_pk'];
        $uzivatel_pk = $this->data['uzivatel_pk'];

        $mTermin = (new Termin())->nactiPodlePk($termin_pk);

        if (null == $mTermin) {
            throw new \Exception("Neexistující termín");
        }

        $hash = TerminPrihlaseni::sestavHash($termin_pk, $uzivatel_pk);

        $detailUrl = \Yii::$app->urlManager->createAbsoluteUrl(['/terminy/default/detail', 'id' => $termin_pk]);
        $prihlaseniUrl = \Yii::$app->urlManager->createAbsoluteUrl(['/terminy/default/prihlasit', 'hash' => $hash]);

        return $this->_mailer->view->render('@app/mail/views/novy-termin', [
            'mTermin' => $mTermin,
            'detailUrl' => $detailUrl,
            'prihlaseniUrl' => $prihlaseniUrl
        ]);
    }

    /**
     * @inheritdoc
     */
    public function vratPrilohy()
    {
        return $this->mTermin->soubory;
    }


}