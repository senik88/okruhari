<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 3. 6. 2015
 * Time: 20:30
 */

namespace app\mail\models;


use app\mail\Email;

/**
 * Class AutomatickePrihlaseniMail
 * @package app\mail\models
 */
class AutomatickePrihlaseniMail extends Email
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->predmet = "Automatické přihlášení";
    }

    /**
     * metoda vezme zaznam z databaze, naplni sablonu daty a vrati vykreslene telo emailu jako string
     *
     * @throws \Exception
     * @return string
     */
    public function vykresliEmail()
    {
        // TODO: Implement _vykresliEmail() method.
    }
}