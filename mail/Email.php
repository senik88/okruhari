<?php
/**
 * Created by PhpStorm.
 * User: senohrabekl
 * Date: 8.1.15
 * Time: 12:44
 */

namespace app\mail;


use app\components\ItemAliasTrait;
use app\modules\admin\models\Soubor;
use Exception;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\swiftmailer\Mailer;

/**
 * Class Email
 * @package app\mail
 *
 * Třídu je vhodné přetížit a doplnit do ní konstanty s typy emailů a ty doplnit do atributu typy.
 * Ve stávající implementaci se s tímto atributem nepočítá, ale invencím se meze nekladou.
 *
 * @property int mail_pk
 * @property string adresat
 * @property string predmet
 * @property string trida
 * @property string stav
 * @property string data
 * @property string cas_vlozeni
 * @property string cas_naplanovani
 * @property string cas_odeslani
 */
abstract class Email extends ActiveRecord
{
    //use ItemAliasTrait;

    const STAV_ODESLANO = 'ODESLANO';
    const STAV_NEODESLANO = 'NEODESLANO';
    const STAV_CHYBA = 'CHYBA';
    const STAV_NOVY = 'NOVY';
    const STAV_ZPRACOVAVA_SE = 'ZPRACOVAVA SE';

    /**
     * @var array oblož pro data
     */
    public $params;

    /**
     * @var
     */
    protected $_error;

    /**
     * @var array
     */
    protected $_typy = [];

    /**
     * @var string cesta k sablone, ze ktere se vytvori telo emailu
     */
    protected $_template;

    /**
     * @var string namespace vlastnich emailovych trid
     */
    protected $_namespace;

    /**
     * @var Mailer
     */
    protected $_mailer;

    /**
     * @var string
     */
    protected $_htmlLayout = '@app/mail/layouts/html';

    /**
     * Konstruktor, konfigurace musi obsahovat polozky "adresat" a "predmet" v poli "data" !!!
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * V initu si predam instanci maileru, protoze disponuje instanci View
     */
    public function init()
    {
        parent::init();

        if ($this->_mailer == null) {
            $this->_mailer = Yii::$app->mailer;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adresat', 'predmet', 'trida', 'stav', 'data', 'cas_naplanovani'], 'required'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'mail';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * Založí záznam pro odeslání mailu
     * Pokud je v datech čas naplánování, uloží email s tímto datem
     *
     * @throws \Exception
     * @return bool
     */
    public function send()
    {
        if (!isset($this->adresat)) {
            throw new Exception("email: nemam vyplaneneho adresata");
        }

        if (!isset($this->predmet)) {
            throw new Exception("email: nemam vyplneny predmet");
        }

        if ($this->cas_naplanovani == null) {
            $this->cas_naplanovani = date('Y-m-d H:i:s', time());
        }

        $params[$this->formName()] = [
            'adresat' => $this->adresat,
            'predmet' => $this->predmet,
            'trida' => get_class($this),
            'stav' => self::STAV_NOVY,
            'data' => Json::encode($this->params),
            'cas_naplanovani' => $this->cas_naplanovani
        ];

        if ($this->load($params)) {
            return $this->save();
        } else {
            throw new Exception("email: nepodarilo se naplnit tridu daty, " . print_r($params, true));
        }
    }

    /**
     * metoda vezme zaznam z databaze, naplni sablonu daty a vrati vykreslene telo emailu jako string
     *
     * @throws \Exception
     * @return string
     */
    public abstract function vykresliEmail();

    /**
     * @return Soubor[]
     */
    public function vratPrilohy()
    {
        return [];
    }

    /**
     * @return string
     */
    protected function render()
    {
        $output = $this->vykresliEmail();
        return $this->_mailer->view->render($this->_htmlLayout, ['content' => $output], $this);
    }

    /**
     * Metoda nainstancuje tridu emailu podle jejiho typu
     *
     * @param array $row
     * @throws \Exception
     * @return void|static
     */
    public static function instantiate($row)
    {
        return new $row['trida'];
    }

    /**
     * @inheritdoc
     * @param Email $record
     * @param array $row
     */
    public static function populateRecord($record, $row)
    {
        parent::populateRecord($record, $row);
        $record->data = Json::decode($row['data']);
    }

    /**
     * Fakticke odeslani emailu, vola metodu pro vykresleni sablony, vyplni prijemce a dalsi pole a odesle email
     * @return bool
     */
    public function odesli()
    {
        try {
            /** @var Message $mail */
            $mail = $this->_mailer->compose();

            if ($this->adresat == 'superadmin') {
                $to = [Yii::$app->params['adminEmail'] => 'Superadmin'];
            } else {
                $to = $this->adresat;
            }

            $mail->setTo($to);
            $mail->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name]);
            $mail->setHtmlBody($this->render());
            $mail->setSubject($this->predmet);

            // metoda vratPrilohy bude vzdy vracet Soubor[], jinak to klekne
            foreach ($this->vratPrilohy() as $mSoubor) {
                if (!($mSoubor instanceof Soubor)) {
                    throw new Exception("priloha neni instanci tridy Soubor");
                }

                $mail->attachContent(
                    file_get_contents($mSoubor->vratCestu() . DIRECTORY_SEPARATOR . $mSoubor->soubor_pk),
                    [
                        'fileName' => $mSoubor->nazev,
                        'contentType' => $mSoubor->typ
                    ]
                );
            }

            if ($this->_mailer->send($mail)) {
                return true;
            } else {
                if (method_exists($this->_mailer, 'getErrorInfo')) {
                    throw new Exception($this->_mailer->getErrorInfo());
                } else {
                    throw new Exception("nepodarilo se odeslat zpravu");
                }
            }
        } catch (\Exception $e) {
            $this->_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Prevadi konstanty na nazev tridy - ALERT_CENA_MAIL => AlertCenaMail
     * @param string $str
     * @param bool $asClass
     * @return string
     */
    protected static function _toCamelCase($str, $asClass = true)
    {
        $camelCase = explode('_', $str);

        for ($i = 0; $i < count($camelCase); $i++) {
            $camelCase[$i] = strtolower($camelCase[$i]);
            if (!$asClass && $i == 0) {
                continue;
            } else {
                $camelCase[$i] = ucfirst($camelCase[$i]);
            }
        }

        return implode('', $camelCase);
    }

    /**
     * getter kvuli db - email nema pk, ale mail_pk
     * @return mixed
     */
    public function getPk()
    {
        return $this->mail_pk;
    }

    /**
     * setter kvuli db - email nema pk, ale mail_pk
     * @param $val
     * @return mixed|void
     */
    public function setPk($val)
    {
        $this->mail_pk = $val;
    }

    /**
     *
     */
    public function odeslano()
    {
        $this->stav = self::STAV_ODESLANO;
        $this->cas_odeslani = date('Y-m-d H:i:s');
        $this->save(false, ['stav', 'cas_odeslani']);
    }

    /**
     *
     */
    public function chyba()
    {
        $this->stav = self::STAV_NEODESLANO;
        $this->save(false, ['stav']);
    }

    /**
     * @throws Exception
     */
    public function zpracuj()
    {
        if ($this->odesli()) {
            $this->odeslano();
        } else {
            $this->chyba();

            $msg = ($this->_error != null ? $this->_error : 'neznama chyba behem zpracovani');
            throw new Exception($msg);
        }
    }
}