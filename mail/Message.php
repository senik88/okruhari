<?php
/**
 * Created by PhpStorm.
 * User: repad
 * Date: 2.12.14
 * Time: 13:11
 */

namespace app\mail;


use yii\mail\BaseMessage;

class Message extends BaseMessage {

    public $charset = 'utf8';
    public $from;
    public $to = [];
    public $replyTo;
    public $cc = [];
    public $bcc = [];
    public $subject;
    public $htmlBody;
    public $textBody;
    public $attachments = [];
    public $images = [];
    /**
     * Returns the character set of this message.
     * @return string the character set of this message.
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * Sets the character set of this message.
     * @param string $charset character set name.
     * @return static self reference.
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
        return $this;
    }

    /**
     * Returns the message sender.
     * @return string the sender
     */
    public function getFrom()
    {
        $email = key($this->from);
        $name = current($this->from);
        if (!is_numeric($name)) {
            return [
                'name' => $name,
                'email' => $email
            ];
        } else {
            return [
                'email' => $name
            ];
        }
    }

    /**
     * Sets the message sender.
     * @param string|array $from sender email address.
     * You may pass an array of addresses if this message is from multiple people.
     * You may also specify sender name in addition to email address using format:
     * `[email => name]`.
     * @return static self reference.
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * Returns the message recipient(s).
     * @return array the message recipients
     */
    public function getTo()
    {
        $toOut = [];
        foreach($this->to as $key => $value) {
            if (is_numeric(($key))) {
                $toOut[] = ['email' => $value, 'type' => 'to'];
            } else {
                $toOut[] = ['email' => $value,'name' => $key, 'type' => 'to'];
            }
        }
        return $toOut;
    }

    /**
     * Sets the message recipient(s).
     * @param string|array $to receiver email address.
     * You may pass an array of addresses if multiple recipients should receive this message.
     * You may also specify receiver name in addition to email address using format:
     * `[email => name]`.
     * @return static self reference.
     */
    public function setTo($to)
    {
        if (!is_array($to))
            $to = [$to];
        $this->to = $to;
        return $this;
    }

    /**
     * Returns the reply-to address of this message.
     * @return string the reply-to address of this message.
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * Sets the reply-to address of this message.
     * @param string|array $replyTo the reply-to address.
     * You may pass an array of addresses if this message should be replied to multiple people.
     * You may also specify reply-to name in addition to email address using format:
     * `[email => name]`.
     * @return static self reference.
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
        return $this;
    }

    /**
     * Returns the Cc (additional copy receiver) addresses of this message.
     * @return array the Cc (additional copy receiver) addresses of this message.
     */
    public function getCc()
    {
        $toOut = [];
        foreach($this->cc as $key => $value) {
            if (is_numeric(($key))) {
                $toOut[] = ['email' => $value, 'type' => 'cc'];
            } else {
                $toOut[] = ['email' => $value,'name' => $key, 'type' => 'cc'];
            }
        }
        return $toOut;
    }

    /**
     * Sets the Cc (additional copy receiver) addresses of this message.
     * @param string|array $cc copy receiver email address.
     * You may pass an array of addresses if multiple recipients should receive this message.
     * You may also specify receiver name in addition to email address using format:
     * `[email => name]`.
     * @return static self reference.
     */
    public function setCc($cc)
    {
        if (!is_array($cc))
            $cc = [$cc];
        $this->cc = $cc;
        return $this;
    }

    /**
     * Returns the Bcc (hidden copy receiver) addresses of this message.
     * @return array the Bcc (hidden copy receiver) addresses of this message.
     */
    public function getBcc()
    {
        $toOut = [];
        foreach($this->bcc as $key => $value) {
            if (is_numeric(($key))) {
                $toOut[] = ['email' => $value, 'type' => 'bcc'];
            } else {
                $toOut[] = ['email' => $value,'name' => $key, 'type' => 'bcc'];
            }
        }
        return $toOut;
    }

    /**
     * Sets the Bcc (hidden copy receiver) addresses of this message.
     * @param string|array $bcc hidden copy receiver email address.
     * You may pass an array of addresses if multiple recipients should receive this message.
     * You may also specify receiver name in addition to email address using format:
     * `[email => name]`.
     * @return static self reference.
     */
    public function setBcc($bcc)
    {
        if (!is_array($bcc))
            $bcc = [$bcc];
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * Returns the message subject.
     * @return string the message subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the message subject.
     * @param string $subject message subject
     * @return static self reference.
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Sets message plain text content.
     * @param string $text message plain text content.
     * @return static self reference.
     */
    public function setTextBody($text)
    {
        $this->textBody = $text;
        return $this;
    }

    /**
     * Sets message HTML content.
     * @param string $html message HTML content.
     * @return static self reference.
     */
    public function setHtmlBody($html)
    {
        $this->htmlBody = $html;
        return $this;
    }
    /**
     * Attaches existing file to the email message.
     * @param string $fileName full file name
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     *
     * @return static self reference.
     */
    public function attach($fileName, array $options = [])
    {
        if (!isset($options['name'])) {
            $name = basename($fileName);
        } else {
            $name = $options['name'];
        }
        return $this->attachContent(file_get_contents($fileName),['name' => $name]);
    }

    /**
     * Attach specified content as file for the email message.
     * @param string $content attachment file content.
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     *
     * @return static self reference.
     */
    public function attachContent($content, array $options = [])
    {
        $this->attachments[] = [
            'type' => 'base64',
            'name' => $options['name'],
            'content' => chunk_split(base64_encode($content)),
        ];
        return $this;
    }

    /**
     * Attach a file and return it's CID source.
     * This method should be used when embedding images or other data in a message.
     * @param string $fileName file name.
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     *
     * @return string attachment CID.
     */
    public function embed($fileName, array $options = [])
    {
        return $this->embedContent(file_get_contents($fileName),$options);
    }

    /**
     * Attach a content as file and return it's CID source.
     * This method should be used when embedding images or other data in a message.
     * @param string $content attachment file content.
     * @param array $options options for embed file. Valid options are:
     *
     * - fileName: name, which should be used to attach file.
     * - contentType: attached file MIME type.
     *
     * @return string attachment CID.
     */
    public function embedContent($content, array $options = [])
    {
        $options['type'] = 'base64';
        $options['name'] = md5(microtime((string)true) . (string)rand(0,1000000));
        $options['content'] = chunk_split(base64_encode($content));
        $this->images[] = $options;
        return $options['name'];
    }

    /**
     * Returns string representation of this message.
     * @return string the string representation of this message.
     */
    public function toString()
    {
        return $this->isHtml() ? $this->htmlBody : $this->textBody;
    }

    public function isHtml() {
        return !empty($this->htmlBody);
    }

    /**
     * Funkce pro podporu developmentu<br>
     * Vymaze ze zpravy vsechny prijemce
     */
    public function clearAllRecipients()
    {
        $this->clearAdresses();
        $this->clearCc();
        $this->clearBcc();
    }

    /**
     * Funkce pro podporu developmentu<br>
     * Vymaze ze zpravy prijemce
     */
    public function clearAdresses()
    {
        $this->setTo([]);
    }

    /**
     * Funkce pro podporu developmentu<br>
     * Vymaze ze zpravy kopie
     */
    public function clearCc()
    {
        $this->setCc([]);
    }

    /**
     * Funkce pro podporu developmentu<br>
     * Vymaze ze zpravy skryte kopie
     */
    public function clearBcc()
    {
        $this->setBcc([]);
    }
}