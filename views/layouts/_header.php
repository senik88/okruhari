<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 22:19
 */

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$user = Yii::$app->user;

NavBar::begin([
    'brandLabel' => 'Termíny Vysoké Mýto',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

echo \app\widgets\AktualniTerminWidget::widget();

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Úvod', 'url' => ['/site/index']],
        ['label' => 'Info', 'url' => ['/site/about']],
        ['label' => 'Termíny', 'url' => ['/terminy/default/index']],
        [
            'visible' => ($user->can('spravovatUzivatele') || $user->can('spravovatTerminy') || $user->can('spravovatNovinky')),
            'label' => 'Administrace',
            'items' => [
                [
                    'label' => 'Dashboard',
                    'url' => ['/admin/default/index'],
                    'visible' => $user->can('spravovatUzivatele')
                ],
                [
                    'label' => 'Uživatelé',
                    'url' => ['/uzivatel/admin/index'],
                    'visible' => $user->can('spravovatUzivatele')
                ],
                [
                    'label' => 'Termíny',
                    'url' => ['/terminy/admin/index'],
                    'visible' => $user->can('spravovatTerminy')
                ],
                [
                    'label' => 'Platby',
                    'url' => ['/terminy/platby/index'],
                    'visible' => $user->can('spravovatPlatby')
                ],
                [
                    'label' => 'Okruhy',
                    'url' => ['/admin/okruhy/index'],
                    'visible' => $user->can('spravovatOkruhy')
                ],
                [
                    'label' => 'Novinky',
                    'url' => ['/admin/novinky/index'],
                    'visible' => $user->can('spravovatNovinky')
                ],
                [
                    'label' => 'Maily v systému',
                    'url' => ['/admin/maily/index'],
                    'visible' => true//$user->can('spravovatNovinky')
                ],
            ]
        ],
        [
            'visible' => !$user->isGuest,
            'label' => $user->isGuest ? 'Nepřihlášen' : $user->identity->login,
            'items' => [
                [
                    'label' => 'Můj profil',
                    'url' => ['/uzivatel/default/muj-profil']
                ],
                [
                    'label' => 'Odhlásit',
                    'url' => ['/uzivatel/default/odhlaseni']
                ],
            ],
        ],
        ['label' => 'Registrace', 'url' => ['/uzivatel/default/registrace'], 'visible' => $user->isGuest],
    ],
]);
NavBar::end();