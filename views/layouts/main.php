<?php
use app\components\Breadcrumbs;
use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$this->registerJs("
    $('select').selectpicker();
");
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
    <link href="<?= Yii::$app->request->baseUrl ?>/favicon.ico" rel="icon" />

    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <div class="wrapperHead">
            <?= $this->render('_header') ?>
        </div>

        <div class="wrapperContent">
            <div class="wrapperMiddle">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>

                <?php
                $flashes = Yii::$app->session->getAllFlashes(true);
                foreach ($flashes as $key => $flash) {
                    echo \yii\bootstrap\Alert::widget(array(
                        'options' => [
                            'class' => "alert-{$key}",
                        ],
                        'body' => is_array($flash) ? implode(', ', $flash) : $flash,
                    ));
                }
                ?>

                <?= $content ?>
            </div>
            <div class="wrapperRight">
                <?= \app\widgets\UzivatelWidget::widget() ?>
                <?= \app\widgets\KalendarWidget::widget([
                    'mesic' => Yii::$app->request->get('mesic', date('n', time())),
                    'rok' => Yii::$app->request->get('rok', date('Y', time())),
                ]) ?>

                <?php
                if (isset($this->blocks['predpoved'])) {
                    echo $this->blocks['predpoved'];
                }
                ?>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
<div class="wrapperFooter">
    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; Lukáš Senohrábek | <a href="http://senovo.cz">Senovo.cz</a> <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
