<?php
/* @var $this yii\web\View */
use yii\bootstrap\Html;

$this->title = Yii::$app->name;
$this->params['breadcrumbs'] = array();
?>

<div class="row">
    <div class="col-md-12 text-center">
        <h1><?= Html::icon('hand-right') ?> Okruhové ježdění Vysoké Mýto <?= Html::icon('hand-left') ?></h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>TODO list</h2>

        <div class="novinka">
            <p>
                !!! Přístup uživatelů na základě rolí.... !!!
            </p>
        </div>
        <div class="novinka">
            <p>
                Zprovoznit odesílání emailů, ať se dá aplikace nasadit a jakž takž provozovat kvůli testování a návrhům...
            </p>
        </div>
        <div class="novinka">
            <p>
                Při uložení nového termínu přidat do systému novinku, aby se zobrazila na homepage...
            </p>
        </div>
        <div class="novinka">
            <p>
                Dodělat práci s platbami - potvrzení přijetí, převedení na kredity...
            </p>
            <p>
                Periodická kontrola přihlášení a plateb, expirace přihlášení po 4 dnech...
            </p>
        </div>
        <div class="novinka">
            <p>
                Doladit upload souborů k termínům...
            </p>
        </div>
        <div class="novinka">
            <p>
                Dodělat správu zbývajících věcí - platby, novinky, maily, okruhy (připravit do budoucna)...
            </p>
        </div>
        <div class="novinka">
            <p>
                Upravit layouty - jedno (administrace, přihlašování, heslo) a dvousloupcový (vše ostatní)...
            </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>Novinky</h2>

        <div class="novinka">
            <p>
                V novinkách zatím nic není, na všem se postupně pracuje...
            </p>
        </div>
    </div>
</div>
