<?php
/**
 * @var $this yii\web\View
 */

$this->title = Yii::$app->name . ' - Informace';
$this->params['breadcrumbs'][] = 'Informace';
?>
<div class="site-about">
    <h1>Informace</h1>

    <p>
        Tohle je vývojová verze, usilovně na tom pracuju...
    </p>

    <p>
        Aplikace slouží k přihlašování jezdců na termíny volného ježdění na okruhu ve Vysokém Mýtě.
    </p>

    <p>
        Aplikaci vyvíjím ve svém volném čase, neslouží k podnikání.
    </p>

    <p>
        Vše děláme jen proto, že nás to baví, a tedy bez nároku na honorář.
    </p>

    <p>.</p><p>.</p><p>.</p>

    <p>
        Mobilní verze bude... někdy :)
    </p>

    <p>
        Náměty, připomínky a pochvaly posílejte na <a href="mailto:lukas.senohrabek@email.cz">lukas.senohrabek(at)email.cz</a>...
    </p>
</div>
