<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 22:26
 */

namespace app\components;


use app\modules\uzivatel\components\UzivatelIdentity;
use app\modules\uzivatel\models\Uzivatel;
use yii\web\IdentityInterface;
use yii\web\User;

/**
 * Class WebUser
 *
 * @property UzivatelIdentity|IdentityInterface|null $identity
 *
 * @package app\components
 */
class WebUser extends User
{

    /**
     * @param string $permissionName
     * @param array $params
     * @param bool $allowCaching
     * @return bool
     */
    public function can($permissionName, $params = [], $allowCaching = true)
    {
        if (\Yii::$app->user->isGuest) {
            return false;
        }

        if (\Yii::$app->user->identity->isAdmin) {
            return true;
        } else {
            return parent::can($permissionName, $params, $allowCaching);
        }
    }

    /**
     * @return bool
     */
    public function getIsAdmin()
    {
        // pokud nejsem prihlaseny, nemuzu byt admin
        if (\Yii::$app->user->isGuest) {
            return false;
        }

        // overim, jestli mam roli superadmina
        if ($this->identity->getRole() == Uzivatel::ROLE_SUPERADMIN) {
            return true;
        }

        // overim, jestli jsem v konfigu mezi administratory
        if (
            isset(\Yii::$app->params['admini'])
            && is_array(\Yii::$app->params['admini'])
            && in_array($this->identity->getUser()->email, \Yii::$app->params['admini'])
        ) {
            return true;
        }

        return false;
    }

}