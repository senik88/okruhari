<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 23.12.2015
 * Time: 9:49
 */

namespace app\components;


use Yii;
use yii\bootstrap\Html;

/**
 * Class Breadcrumbs
 * @package app\components
 */
class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    /**
     * Renders the widget.
     */
    public function run()
    {
        $links = [];
        if ($this->homeLink === null) {
            $links[] = $this->renderItem([
                'label' => Yii::t('yii', 'Home'),
                'url' => Yii::$app->homeUrl,
            ], $this->itemTemplate);
        } elseif ($this->homeLink !== false) {
            $links[] = $this->renderItem($this->homeLink, $this->itemTemplate);
        }
        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }
            $links[] = $this->renderItem($link, isset($link['url']) ? $this->itemTemplate : $this->activeItemTemplate);
        }
        echo Html::tag($this->tag, implode('', $links), $this->options);
    }
}