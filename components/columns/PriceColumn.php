<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 27.12.14
 * Time: 10:22
 */

namespace app\components\columns;


use Closure;
use yii\grid\DataColumn;
use Yii;

class PriceColumn extends DataColumn
{
    /**
     * @var
     */
    public $mena;

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $formatter = Yii::$app->formatter;

        if ($this->mena == null) {
            $this->mena = $formatter->currencyCode;
        }

        $value = $this->getDataCellValue($model, $key, $index);

        return number_format($value, 2, ',', '&nbsp;') . '&nbsp;' . $this->mena;
    }

    /**
     * Renders a data cell.
     * @param mixed $model the data model being rendered
     * @param mixed $key the key associated with the data model
     * @param integer $index the zero-based index of the data item among the item array returned by [[GridView::dataProvider]].
     * @return string the rendering result
     */
    public function renderDataCell($model, $key, $index)
    {
        if (!($this->contentOptions instanceof Closure)) {
            $this->contentOptions = array_merge($this->contentOptions, array('class' => 'price-column'));
        }
        return parent::renderDataCell($model, $key, $index);
    }
} 