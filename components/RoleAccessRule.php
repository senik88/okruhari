<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 13. 12. 2015
 * Time: 14:17
 */

namespace app\components;


use app\modules\uzivatel\components\UzivatelIdentity;
use app\modules\uzivatel\models\Uzivatel;
use yii\filters\AccessRule;

/**
 * Class RoleAccessRule
 * @package app\components
 */
class RoleAccessRule extends AccessRule
{
    /**
     * @param WebUser $user the user object
     * @return boolean whether the rule applies to the role
     */
    protected function matchRole($user)
    {
        if (empty($this->roles)) {
            return true;
        }

        if ($user->getIsAdmin()) {
            return true;
        }

        foreach ($this->roles as $role) {
            if ($role == '?') {
                if ($user->getIsGuest()) {
                    return true;
                }
            } elseif ($role == Uzivatel::ROLE_USER) {
                if (!$user->getIsGuest()) {
                    return true;
                }
                // Check if the user is logged in, and the roles match
            } elseif (!$user->getIsGuest() && $role == $user->identity->getRole()) {
                return true;
            }
        }

        return false;
    }
}