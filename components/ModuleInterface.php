<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 5.10.14
 * Time: 22:50
 */

namespace app\components;

/**
 * Interface ModuleInterface
 * @package components
 */
interface ModuleInterface
{

    public function vratMenuModulu();

}