<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 1.11.14
 * Time: 14:21
 */

namespace app\components\helpers;


class DbUtils
{

    /**
     * todo trochu poladit - volat quote atp
     * @param $data
     * @param null $type
     * @return bool|string
     */
    public static function implodePgArray($data, $type = null)
    {
        if (!empty($data)) {
            $to_array = array();
            foreach ($data as $one) {
                if (is_array($one)) {
                    $to_array[] = self::implodePgArray($one);
                } else {
                    $to_array[] = $one;
                }
            }

            $pgArray = '{' . implode(',', $to_array) . '}';

            return $pgArray;
        } else {
            return false;
        }
    }

    /**
     * @param $string
     * @return array
     */
    public static function explodePgArray($string)
    {
        $string = trim($string, '{}');
        if ('NULL' == $string) {
            return array();
        } else {
            return explode(',', $string);
        }
    }

} 