<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 17.10.14
 * Time: 17:39
 */

namespace app\components;


use Yii;

class Application
{

    /**
     * doimplementovat
     */
    public static function log($message, $level)
    {

    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashError($message, $remove = true)
    {
        $type = 'danger';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashSuccess($message, $remove = true)
    {
        $type = 'success';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashInfo($message, $remove = true)
    {
        $type = 'info';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $message
     * @param bool $remove
     */
    public static function setFlashWarning($message, $remove = true)
    {
        $type = 'warning';
        self::setFlash($type, $message, $remove);
    }

    /**
     * @param string $type
     * @param string $message
     * @param bool $remove
     */
    protected static function setFlash($type, $message, $remove)
    {
        Yii::$app->session->addFlash($type, $message, $remove);
    }

} 