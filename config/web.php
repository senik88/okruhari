<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$mailer = require(__DIR__ . '/_mailer.php');

$config = [
    'id' => 'terminy-vmyto',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'cs-CZ',
    'name' => 'Termíny Vysoké Mýto',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'kyyZwCyAOWec4-yMaz6Vj5MsskZ5xStk',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'app\modules\uzivatel\components\UzivatelIdentity',
            'enableAutoLogin' => true,
            'loginUrl' => ['/site/index']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'locale' => 'cs-CZ'
        ],
        'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'logFile' => '@runtime/logs/app-info.log',
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                [
                    'logFile' => '@runtime/logs/app-error.log',
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                    'logVars' => ['_POST'],
                ],
                [
                    'logFile' => '@runtime/logs/app-warning.log',
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['warning'],
                    'logVars' => ['_POST'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            //'urlFormat' => 'path',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // your rules go here // klasickej preklad url
                // uzivatelModule
                '/prihlaseni' => '/uzivatel/default/prihlaseni',
                '/muj-profil/<tab:\w+>' => '/uzivatel/default/muj-profil', // budu tam delat zalozky?
                '/muj-profil' => '/uzivatel/default/muj-profil',
                '/aktivace/<klic:[a-zA-Z0-9]+>' => '/uzivatel/default/aktivace',

                // terminyModule
                '/terminy' => '/terminy/default/index',
                '/termin/prihlasit/<hash:\w+>' => '/terminy/default/prihlasit',
                '/termin/odhlasit/<hash:\w+>' => '/terminy/default/odhlasit',

                // adminModule

                // statické
                '/informace' => '/site/about',
                '/' => '/site/index',
            ],
        ],
    ],
    'modules' => [
        'redactor' => 'yii\redactor\RedactorModule',
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'uzivatel' => [
            'class' => 'app\modules\uzivatel\UzivatelModule',
            'name' => 'Uživatelé'
        ],
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
            'name' => 'Administrace'
        ],
        'terminy' => [
            'class' => 'app\modules\terminy\TerminyModule',
            'name' => 'Termíny'
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
