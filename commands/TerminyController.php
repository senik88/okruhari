<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 22.12.2015
 * Time: 22:58
 */

namespace app\commands;


use app\modules\terminy\models\Termin;
use yii\console\Controller;

/**
 * Class TerminyCommand
 *
 * Stara se o backendovou praci s terminy
 * - zverejneni terminu
 * - kontrola a expirace prihlaseni
 *
 * @package app\commands
 */
class TerminyController extends Controller
{
    /**
     * @var integer pocet vterin, na jak dlouho se ma manager uspat pred ukoncenim
     */
    protected $sleep_seconds = 120;

    /**
     * Zverejni vsechny terminy, ktere jsou ve stavu NOVY a cas zverejneni maji < now()
     * @param int|null $limit
     * @return int
     */
    public function actionZverejni($limit = null)
    {
        $sql = "SELECT termin_pk AS pk FROM termin WHERE stav = :novy AND zverejnit <= now()";
        $params = [
            ':novy' => Termin::STAV_NOVY
        ];

        if (null != $limit) {
            $sql .= " limit :limit";
            $params[':limit'] = $limit;
        }

        $terminy = \Yii::$app->db->createCommand($sql, $params)->queryAll();

        if (count($terminy) == 0) {
            \Yii::info("zadne terminy ke zverejneni");
        } else {
            \Yii::info("pocet terminu ke zverejneni: " . count($terminy));
            foreach ($terminy as $termin) {
                try {
                    $mTermin = (new Termin())->nactiPodlePk($termin['pk']);

                    // tohle idealne dat do modelu Termin, at to nestrasi v controlleru, kde to nema co delat...
                    $terminSql = "UPDATE termin SET stav = :verejny WHERE termin_pk = :pk";
                    $pocet = \Yii::$app->db->createCommand($terminSql, [
                        ':verejny' => Termin::STAV_VEREJNY,
                        ':pk' => $mTermin->termin_pk
                    ])->execute();

                    if ($pocet != 1) {
                        throw new \Exception("neplatny pocet updatovanych radku ({$pocet}) pri zverejneni terminu s pk = {$mTermin->termin_pk}");
                    }
                } catch (\Exception $e) {
                    \Yii::error("nepodarilo se zverejnit termin s pk = {$termin['pk']}, chyba: {$e->getMessage()}");
                }
            }

            \Yii::info("vsechny terminy byly zverejneny");
        }

        // na jak dlouho se mam uspat?
        $this->nastavPocetVterinPredNejblizsimTerminem();
        return Controller::EXIT_CODE_NORMAL;
    }

    /**
     * Najde vsechny prihlasene - je jedno na ktery termin, vzdy ale VEREJNY > now()
     * - Bude se spoustet vzdy kratce po pulnoci CRONem
     * - Prihlasenemu poslu informaci 1 den pred expiraci, pak 12h pred expiraci
     * - Organizatorovi poslu email, ze si ma zkontrolovat platby 1 den a 12h pred expiraci
     */
    public function actionExpirujPrihlaseni()
    {
        $db = \Yii::$app->db;

        $transakce = $db->beginTransaction();


        $transakce->rollBack();
    }

    /**
     *
     */
    protected function nastavPocetVterinPredNejblizsimTerminem()
    {
        $sql = "select min(extract(epoch from zverejnit - now())::int) from termin where stav = :novy";
        $params = [
            ':novy' => Termin::STAV_NOVY
        ];

        $sekund = \Yii::$app->db->createCommand($sql, $params)->queryScalar();

        if ($sekund < 0) {
            $this->sleep_seconds = 0;
        } else if ($sekund < $this->sleep_seconds) {
            $this->sleep_seconds = $sekund;
        }
    }

    /**
     * Uspani managera, aby se nespustil hned
     *
     * @param \yii\base\Action $action
     * @param mixed $result
     * @return mixed|void
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        // pokud probehlo vse v poradku, tak se prospim
        if ($result == Controller::EXIT_CODE_NORMAL) {
            sleep($this->sleep_seconds);
        }

        return $result;
    }

}