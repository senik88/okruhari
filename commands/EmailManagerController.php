<?php
/**
 * Created by PhpStorm.
 * User: repad
 * Date: 6.1.15
 * Time: 14:34
 */

namespace app\commands;


use app\mail\Email;
use app\mail\EmailManager;
use Exception;
use yii\console\Controller;
use Yii;
use yii\log\Logger;

/**
 * Obecny manager slouzici pro zpracovani naplanovanych uloh a jejich oznaci podle vysledneho stavu ulohy
 * Class KomunikacniManagerController
 * @package comgate\manager
 */
class EmailManagerController extends Controller
{
    public $logStart = 'Start zpracovani davky uloh';
    public $logKonec = 'Konec zpracovani davky uloh';

    public $obsluhaClass = 'app\mail\EmailManager';

    /**
     * @var Logger
     */
    protected $_log;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        if ($this->_log == null) {
            $this->_log = Yii::getLogger();
        }
    }

    /**
     * akce slouzi pro zpracovani vsech nezpracovanych uloh a nastaveni stavu podle vysledku ulohy
     */
    public function actionSpustDavku()
    {
        $this->_log->log($this->logStart, Logger::LEVEL_INFO);

        try {
            $mObsluha = new EmailManager();
            /** @var Email[] $ulohy */
            $ulohy = $mObsluha->vratDavku();

            if (empty($ulohy)) {
                $this->_log->log("Žádné úlohy ke zpracování", Logger::LEVEL_INFO);
            } else {
                foreach ($ulohy as $mUloha) {
                    try {
                        $mUloha->zpracuj();
                    } catch (Exception $e) {
                        $this->_log->log("Nepodařilo se zpracovat ulohu " . get_class($mUloha) . ", uloha_pk = {$mUloha->pk}, {$e->getMessage()}", Logger::LEVEL_ERROR);
                    }
                }
            }
        } catch (Exception $e) {
            $this->_log->log('Nepodařilo se zpracovat davku uloh: ' . $e->getMessage(), Logger::LEVEL_ERROR);

            return Controller::EXIT_CODE_ERROR;
        }

        $this->_log->log($this->logKonec, Logger::LEVEL_INFO);

        return Controller::EXIT_CODE_NORMAL;
    }

    /**
     * @param $pk
     * @return int
     */
    public function actionSpustJednotlive($pk)
    {
        $mObsluha = new EmailManager();

        try {
            $mUloha = $mObsluha->najdiUlohu($pk);
            $mUloha->zpracuj();
        } catch (Exception $e) {
            $this->_log->log("Nepodařilo se zpracovat ulohu uloha_pk = {$pk}, {$e->getMessage()}", Logger::LEVEL_ERROR);
            return Controller::EXIT_CODE_ERROR;
        }
        return Controller::EXIT_CODE_NORMAL;
    }
}