<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 17:52
 */

namespace app\widgets;


use app\modules\terminy\models\Termin;
use yii\bootstrap\Widget;

/**
 * Class AktualniTerminWidget
 * @package app\widgets
 */
class AktualniTerminWidget extends Widget
{
    /**
     * todo implement me
     */
    public function run()
    {
        /** @var Termin $mTermin */
        $mTermin = (new Termin())->vratNejblizsi();

        return $this->render('aktualni-termin', array(
            'mTermin' => $mTermin
        ));
    }
}