<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 20:56
 */

namespace app\widgets;


use app\modules\uzivatel\models\Uzivatel;
use yii\bootstrap\Widget;

/**
 * Class UzivatelWidget
 * @package app\widgets
 */
class UzivatelWidget extends Widget
{
    /**
     * @var Uzivatel
     */
    protected $_uzivatel;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (\Yii::$app->user->isGuest) {
            $this->_uzivatel = new Uzivatel(['scenario' => 'login']);
        } else {
            $this->_uzivatel = Uzivatel::findOne(\Yii::$app->user->id);
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->runPrihlaseni();
        } else {
            return $this->runProfil();
        }
    }

    /**
     * @return string
     */
    protected function runPrihlaseni()
    {
        return $this->render('_prihlaseni', [
            'mUzivatel' => $this->_uzivatel
        ]);
    }

    /**
     * @return string
     */
    protected function runProfil()
    {
        $terminu = $this->_uzivatel->vratPocetOdjetychTerminu();

        return $this->render('_profil', [
            'mUzivatel' => $this->_uzivatel,
            'terminu' => $terminu
        ]);
    }
}