<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 21:10
 *
 * @var $mUzivatel Uzivatel
 */
use app\modules\uzivatel\models\Uzivatel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div id="uzivatel-widget" class="prihlaseni">
    <span class="heading">Přihlášení</span>

    <?php
    $form = ActiveForm::begin([
        'id' => 'prihlaseni-form',
        'action' => ['/uzivatel/default/prihlaseni'],
        'method' => 'POST',
        'enableClientValidation' => true
    ]); ?>
    <div class="form-fields">
        <?= $form->field($mUzivatel, 'email')->textInput([
            'placeholder' => $mUzivatel->getAttributeLabel('email')
        ])->label(false) ?>
        <?= $form->field($mUzivatel, 'heslo')->passwordInput([
            'placeholder' => $mUzivatel->getAttributeLabel('heslo')
        ])->label(false) ?>
        <?= $form->field($mUzivatel, 'zapamatovat')->checkbox() ?>
    </div>

    <div class="form-actions">
        <?= Html::submitButton('Přihlásit', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Zapomenuté heslo', Yii::$app->getUrlManager()->createAbsoluteUrl('/uzivatel/default/zapomenute-heslo'), ['class' => 'btn btn-link']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>