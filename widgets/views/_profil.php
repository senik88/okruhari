<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 21:10
 *
 * @var $mUzivatel Uzivatel
 * @var $terminu integer
 */
use app\modules\uzivatel\models\Uzivatel;
use yii\helpers\Url;

?>

<div id="uzivatel-widget" class="profil">
    <span class="heading">Profil</span>
    <ul>
        <li><?= sprintf("%s %s (%s)", $mUzivatel->prijmeni, $mUzivatel->jmeno, $mUzivatel->nick) ?></li>
        <li>Byl jsem na <?= $terminu ?> termínech</li>
        <li>Mám v banku: <?= Yii::$app->formatter->asCurrency($mUzivatel->ucet_kredity) ?></li>
        <li>Více v <a href="<?= Url::to(['/uzivatel/default/muj-profil']) ?>">mém profilu</a></li>
    </ul>
</div>
