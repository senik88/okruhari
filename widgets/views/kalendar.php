<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 21:01
 */

$monthNames = Array("Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec");

$cMonth = Yii::$app->request->get('mesic', date("n"));
$cYear  = Yii::$app->request->get('rok', date("Y"));

$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
    $prev_month = 12;
    $prev_year = $cYear - 1;
}
if ($next_month == 13 ) {
    $next_month = 1;
    $next_year = $cYear + 1;
}

$queryParamsN = $queryParamsP = Yii::$app->request->getQueryParams();

$queryParamsN[] = Yii::$app->request->getPathInfo();
$queryParamsN['rok'] = $next_year;
$queryParamsN['mesic'] = $next_month;

$queryParamsP[] = Yii::$app->request->getPathInfo();
$queryParamsP['rok'] = $prev_year;
$queryParamsP['mesic'] = $prev_month;
?>


<div id="kalendar-widget">
    <span class="heading">Kalendář</span>
    <table class="kalendar">
        <tr>
            <td style="text-align: center;">
                <a href="<?= Yii::$app->urlManager->createAbsoluteUrl($queryParamsP); ?>">&lt;&lt;</a>
            </td>
            <td colspan="5" class="mesic">
                <strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong>
            </td>
            <td style="text-align: center;">
                <a href="<?= Yii::$app->urlManager->createAbsoluteUrl($queryParamsN); ?>">&gt;&gt;</a>
            </td>
        </tr>
        <tr class="oddelovac"></tr>
        <tr class="kalendar-dny">
            <td class="den"><strong>Po</strong></td>
            <td class="den"><strong>Út</strong></td>
            <td class="den"><strong>St</strong></td>
            <td class="den"><strong>Čt</strong></td>
            <td class="den"><strong>Pá</strong></td>
            <td class="den"><strong>So</strong></td>
            <td class="den"><strong>Ne</strong></td>
        </tr>
        <tr class="oddelovac"></tr>
        <?php
        $timestamp = mktime(0,0,0,$cMonth,1,$cYear);
        $maxday = date("t",$timestamp);
        $thismonth = getdate($timestamp);
        $startday = ($thismonth['wday'] == 0 ? 7 : $thismonth['wday']) - 1;

        for ($i=0; $i<($maxday+$startday); $i++) {

            if (($i % 7) == 0 ) {
                echo "<tr>";
            }

            if ($i < $startday) {
                echo "<td></td>";
            } else {
                $den = ($i - $startday + 1);
                $class = array();
                $akceDen = '';

                if (isset($akceMesic[$den])) {
                    $class[] = 'akce';
                    $akceDen = '<div class="seznam-akci-'.$den.' zavren">';
                    $akceDen .= '<span>seznam akcí</span>';
                    $akceDen .= '<ul>';
                    foreach ($akceMesic[$den] as $jedna) {
                        $akceDen .= '<li><a href="'.Yii::$app->urlManager->createAbsoluteUrl(array(Yii::$app->request->getPathInfo(), 'meta' => $jedna['meta'])).'">'.$jedna['nazev'].'</a></li>';
                    }
                    $akceDen .= '</ul></div>';
                }

                if ($den == date('j') && $cMonth == date('n') && $cYear == date('Y')) {
                    $class[] = 'dnes';
                }

                echo "<td><span class='".implode(' ', $class)."'>{$den}</span>{$akceDen}</td>";
            }

            if (($i % 7) == 6 ) {
                echo "</tr>";
            }
        }
        ?>
    </table>
</div>