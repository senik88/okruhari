<?php
/**
 * Created by PhpStorm.
 * User: Senik
 * Date: 11. 12. 2015
 * Time: 14:49
 *
 * @var $mTermin Termin
 */

use app\modules\terminy\models\Termin;
use yii\helpers\Html;

?>

<div id="aktualni-termin" class="navbar-nav">
    <?php if ($mTermin == null) { ?>
        <div class="zadny-termin">
            Aktuálně není vypsaný žádný termín.
        </div>
    <?php } else { ?>
        <div class="termin-info">
            Nejbližší vypsaný termín:
        </div>
        <div class="termin-info">
            <?= Html::a(
                sprintf(
                    '%s, kapacita: %d/%d',
                    Yii::$app->formatter->asDatetime($mTermin->cas_od),
                    $mTermin->kapacita - $mTermin->obsazeno,
                    $mTermin->kapacita
                ),
                ['/terminy/default/detail', 'id' => $mTermin->termin_pk]
            ) ?>
        </div>
    <?php } ?>
</div>
