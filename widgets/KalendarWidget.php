<?php
/**
 * Created by PhpStorm.
 * User: Lukáš
 * Date: 1. 6. 2015
 * Time: 20:54
 */

namespace app\widgets;


use app\modules\terminy\models\Termin;
use yii\bootstrap\Widget;

/**
 * Class KalendarWidget
 * @package app\widgets
 */
class KalendarWidget extends Widget
{
    /**
     * @var
     */
    public $mesic;

    /**
     * @var
     */
    public $rok;

    public $monthNames = ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"];

    /**
     * @return string
     */
    public function run()
    {
        $akce = $this->vratAkceVmesici($this->mesic, $this->rok);

        return $this->render('kalendar', array(
            'akceMesic' => $akce
        ));
    }

    /**
     * @param null $mesic
     * @param null $rok
     * @return array
     */
    protected function vratAkceVmesici($mesic = null, $rok = null)
    {
        $sql = "
          select
              t.termin_pk as id
            , 'název' as nazev
            , 'test' as meta
            , extract(day from t.cas_od) as konani_od
            , extract(day from t.cas_do) as konani_do
            from termin t
            where t.cas_od >= :od
              and t.cas_do <= :do
              and t.stav = :verejny
        ";

        // todo wtf??? no tak to bude ještě sranda...
        $params['od'] = date("Y-m-01", strtotime("{$rok}-{$mesic}")) . ' 00:00:00';
        $params['do'] = date("Y-m-t", strtotime("{$rok}-{$mesic}")) . ' 23:59:59';

        $params[':verejny'] = Termin::STAV_VEREJNY;

        $result = \Yii::$app->db->createCommand($sql)->bindValues($params)->queryAll();

        $dnyAkci = $akceMesic = array();
        foreach ($result as $akce) {
            $od = (int)$akce['konani_od'];
            $do = (int)$akce['konani_do'];
            for ($i = $od; $i <= $do; $i++) {
                $dnyAkci[] = $i;
                $akceMesic[$i][] = $akce;
            }
        }

        return $akceMesic;
    }
}