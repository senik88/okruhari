begin;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;


-- nejprve create script
\i ./create/create.sql
\i ./create/auth.sql

-- musim nahrat procedury
\i ./procs/f_utils.sql
\i ./procs/f_uzivatel.sql
\i ./procs/f_terminy.sql

-- pak ho naladuju daty
\i ./data/init_data.sql