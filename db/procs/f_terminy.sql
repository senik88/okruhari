
-- // vytvoreni nebo zmena terminu // --
CREATE OR REPLACE FUNCTION termin_zmena(
    a_termin_pk   termin.termin_pk%TYPE
  , a_cas_od      termin.cas_od%TYPE
  , a_cas_do      termin.cas_do%TYPE
  , a_zverejnit   termin.zverejnit%TYPE
  , a_stav        termin.stav%TYPE
  , a_popis       termin.popis%TYPE
  , a_kapacita    termin.kapacita%TYPE
  , a_vypsal      termin.uzivatel_pk%TYPE
  , a_cena        termin.cena%TYPE
) RETURNS integer as $func$
/**
 * navratove hodnoty:
 * >1   vse v poradku, vracim termin_pk
 * -1   neexistujici termin
 * -2   neexistujici uzivatel
 */
DECLARE
  lr_termin   termin%ROWTYPE;
  l_termin_pk termin.termin_pk%TYPE;
BEGIN

  IF NOT EXISTS(SELECT 1 FROM uzivatel WHERE uzivatel_pk = a_vypsal) THEN
    RETURN -2;
  END IF;

  IF a_termin_pk IS NULL THEN

    l_termin_pk := nextval('termin_termin_pk_seq'::REGCLASS);

    INSERT INTO termin (termin_pk, cas_od, cas_do, stav, popis, kapacita, uzivatel_pk, zverejnit, cena) VALUES
      (l_termin_pk, a_cas_od, a_cas_do, a_stav, a_popis, a_kapacita, a_vypsal, a_zverejnit, a_cena);

  ELSE

    IF NOT EXISTS(SELECT 1 FROM termin WHERE termin_pk = a_termin_pk) THEN
      RETURN -1;
    END IF;

    UPDATE termin SET
        cas_od = a_cas_od
      , cas_do = a_cas_do
    WHERE termin_pk = a_termin_pk RETURNING termin_pk INTO l_termin_pk;

  END IF;

  RETURN l_termin_pk;

END;
$func$ LANGUAGE 'plpgsql';
-- //


-- // prihlaseni uzivatele na termin // --
CREATE OR REPLACE FUNCTION uzivatel_prihlas_termin(
      a_termin_pk   termin_prihlaseni.termin_pk%TYPE
    , a_uzivatel_pk termin_prihlaseni.termin_pk%TYPE
    , OUT r_prihlaseni_pk termin_prihlaseni.termin_prihlaseni_pk%TYPE
    , OUT r_popis TEXT
) RETURNS RECORD AS $func$
/**
 * Pri odhlaseni nemusim nic navic delat, protoze se mi uvolnilo misto na presun nahradnika
 */
DECLARE
  lr_termin_prihlaseni  termin_prihlaseni%ROWTYPE;
  lr_termin             RECORD;
  lr_platba             platba%ROWTYPE;

  l_stav_prihlaseni     termin_prihlaseni.stav%TYPE;
BEGIN

    SELECT
        s.*
        , (s.kapacita - s.prihlaseno) AS volno
    INTO lr_termin
    FROM (
             SELECT
                 t.termin_pk
                 , t.cas_od::TIMESTAMPTZ
                 , t.cas_do::TIMESTAMPTZ
                 , t.kapacita
                 , count(tp.termin_pk) as prihlaseno
                 , t.stav
             FROM termin t
                 LEFT JOIN termin_prihlaseni tp ON t.termin_pk = tp.termin_pk AND tp.stav IN ('PRIHLASENY', 'POTVRZENY')
             WHERE t.termin_pk = a_termin_pk
             GROUP BY t.termin_pk
         ) AS s
    ;

    IF NOT FOUND THEN
        r_prihlaseni_pk = -1;
        r_popis = 'Neexistujici termin';
        RETURN;
    END IF;

    IF (lr_termin.cas_od::TIMESTAMPTZ < now()) OR (lr_termin.stav = 'STORNO') THEN
        r_prihlaseni_pk = -2;
        r_popis = 'Stary nebo stornovany termin';
        RETURN;
    END IF;

    SELECT * INTO lr_termin_prihlaseni FROM termin_prihlaseni WHERE termin_pk = a_termin_pk AND uzivatel_pk = a_uzivatel_pk;

    -- prihlaseni na termin jeste neexistuje, takze uzivatele prihlasim
    -- nebo jsem nahradnik a je volno, tak se prehlasim
    IF NOT FOUND OR (lr_termin_prihlaseni.stav = 'NAHRADNIK' AND lr_termin.volno > 0) THEN
        INSERT INTO termin_prihlaseni (termin_pk, uzivatel_pk, platba_pk, stav, cas_prihlaseni) VALUES
            (a_termin_pk, a_uzivatel_pk, NULL, (CASE WHEN lr_termin.volno > 0 THEN 'PRIHLASENY' ELSE 'NAHRADNIK' END), now())
        RETURNING * INTO lr_termin_prihlaseni;

        -- pro nahradnika nebudu pripravovat platbu
        IF lr_termin_prihlaseni.stav = 'PRIHLASENY' THEN
            -- pripravim platbu
            PERFORM termin_zaloz_platbu(a_termin_pk, a_uzivatel_pk);
        END IF;

        r_prihlaseni_pk = lr_termin_prihlaseni.termin_prihlaseni_pk;
        r_popis = 'OK';
        RETURN;

    -- pokud uz jsem prihlaseny, tak se nemuzu prihlasit znovu
    ELSIF lr_termin_prihlaseni.stav IN ('PRIHLASENY', 'POTVRZENY') OR (lr_termin_prihlaseni.stav = 'NAHRADNIK' AND lr_termin.volno = 0) THEN
        r_prihlaseni_pk = -3;
        r_popis = 'Uzivatel uz je na termin prihlasen se stavem ' || lr_termin_prihlaseni.stav;
        RETURN;

    -- stara funkcionalita, nemelo by nastat
    ELSIF lr_termin_prihlaseni.stav = 'ZRUSENY' THEN

        DELETE FROM termin_prihlaseni WHERE termin_prihlaseni.termin_prihlaseni_pk = lr_termin_prihlaseni.termin_prihlaseni_pk;

        SELECT * INTO r_prihlaseni_pk, r_popis FROM uzivatel_prihlas_termin(a_termin_pk, a_uzivatel_pk);
        RETURN;
    END IF;

    -- nepovedlo se, nekde je chyba
    r_prihlaseni_pk = -4;
    r_popis = 'Neznama chyba';
    RETURN;
END;
$func$ LANGUAGE 'plpgsql';
-- //


-- // odhlaseni uzivatele z terminu // --
CREATE OR REPLACE FUNCTION uzivatel_odhlas_termin(
    a_termin_pk   termin_prihlaseni.termin_pk%TYPE
  , a_uzivatel_pk termin_prihlaseni.termin_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  lr_termin_prihlaseni  termin_prihlaseni%ROWTYPE;
  lr_platba             platba%ROWTYPE;
BEGIN
    -- muj termin
    SELECT * INTO lr_termin_prihlaseni FROM termin_prihlaseni WHERE termin_pk = a_termin_pk AND uzivatel_pk = a_uzivatel_pk;

    IF NOT FOUND THEN
        RETURN -1;
    END IF;

    IF lr_termin_prihlaseni.platba_pk IS NOT NULL THEN
        -- musim stornovat platbu
        SELECT * INTO lr_platba FROM platba WHERE platba_pk = lr_termin_prihlaseni.platba_pk;

        IF FOUND THEN

            IF lr_platba.stav = 'ZAPLACENA' THEN
                -- tady bych ji mel prevest na kredity
                PERFORM uzivatel_ucet_zmena(a_uzivatel_pk, lr_platba.castka_zaplacena, 'Prevedeni platby na kredity', lr_platba.platba_pk, NULL);

            END IF;

            UPDATE platba SET stav = 'ZRUSENA' WHERE platba.platba_pk = lr_platba.platba_pk;

        END IF;

    END IF;

    -- prihlaseni smazu, at se mi nemicha poradi nahradniku
    DELETE FROM termin_prihlaseni WHERE termin_pk = a_termin_pk AND uzivatel_pk = a_uzivatel_pk;

  -- prvni nahradnik
  SELECT * INTO lr_termin_prihlaseni FROM termin_prihlaseni WHERE termin_pk = a_termin_pk AND stav = 'NAHRADNIK' ORDER BY cas_prihlaseni ASC LIMIT 1;

  IF FOUND THEN
    PERFORM uzivatel_prihlas_termin(lr_termin_prihlaseni.termin_pk, lr_termin_prihlaseni.uzivatel_pk);
    RETURN lr_termin_prihlaseni.termin_prihlaseni_pk;
  END IF;

  RETURN 0;
END;
$func$ LANGUAGE 'plpgsql';
-- //


-- // zalozeni platby // --
CREATE OR REPLACE FUNCTION termin_zaloz_platbu(
    a_termin_pk termin.termin_pk%TYPE
  , a_uzivatel_pk uzivatel.uzivatel_pk%TYPE
) RETURNS INTEGER AS $func$
/**
 * navratove hodnoty
 */
DECLARE
  lr_termin_prihlaseni termin_prihlaseni%ROWTYPE;
  lr_termin termin%ROWTYPE;
  lr_platba platba%ROWTYPE;

  l_kod integer;
  l_pk  platba.platba_pk%TYPE;
BEGIN

  SELECT * INTO lr_termin_prihlaseni FROM termin_prihlaseni WHERE termin_pk = a_termin_pk AND uzivatel_pk = a_uzivatel_pk;
  IF NOT FOUND THEN
    RETURN -1;

  END IF;

  SELECT * INTO lr_termin FROM termin WHERE termin_pk = a_termin_pk;
  IF NOT FOUND THEN
    RETURN -2;

  END IF;

  -- pokud uz platba existuje, tak ji restartuju
  IF lr_termin_prihlaseni.platba_pk IS NOT NULL THEN
    SELECT * INTO lr_platba FROM platba WHERE platba_pk = lr_termin_prihlaseni.platba_pk;

    l_pk := platba_zmena(lr_termin_prihlaseni.platba_pk, lr_termin.cena, lr_platba.castka_zaplacena, null, null, 'OCEKAVANA', lr_platba.kod);

  ELSE
    -- loop delam kvuli zacykleni, i kdyz to snad hrozit nebude...
    FOR i IN 1..10 LOOP
      l_kod := get_random_number(1000000, 999999999);

      IF NOT EXISTS(SELECT 1 FROM platba WHERE kod = l_kod) THEN
        l_pk := platba_zmena(null, lr_termin.cena, null, null, null, 'OCEKAVANA', l_kod);
        EXIT;
      END IF;

    END LOOP;

  END IF;

  IF l_pk IS NULL THEN
    RETURN -5;
  ELSE
    UPDATE termin_prihlaseni tp SET platba_pk = l_pk WHERE tp.termin_prihlaseni_pk = lr_termin_prihlaseni.termin_prihlaseni_pk;
    RETURN l_pk;
  END IF;
END;
$func$ LANGUAGE 'plpgsql';
-- //


-- // zmena platby // --
CREATE OR REPLACE FUNCTION platba_zmena(
    a_platba_pk         platba.platba_pk%TYPE
  , a_castka            platba.castka%TYPE
  , a_castka_zaplacena  platba.castka_zaplacena%TYPE
  , a_datum             platba.datum%TYPE
  , a_refundace         platba.refundace%TYPE
  , a_stav              platba.stav%TYPE
  , a_kod               platba.kod%TYPE
) RETURNS INTEGER AS $func$
DECLARE
  l_pk platba.platba_pk%TYPE;
BEGIN

  IF a_platba_pk IS NULL THEN
    INSERT INTO platba (castka, datum, refundace, stav, kod) VALUES
      (a_castka, a_datum, a_refundace, a_stav, a_kod) RETURNING platba_pk INTO l_pk;

  ELSE
    UPDATE platba SET
        castka = a_castka
        , castka_zaplacena = a_castka_zaplacena
      , datum = a_datum
      , refundace = a_refundace
      , stav = a_stav
      , kod = a_kod
    WHERE platba_pk = a_platba_pk RETURNING platba_pk INTO l_pk;

  END IF;

  RETURN l_pk;

END;
$func$ LANGUAGE 'plpgsql';
-- //


-- // potvrzeni platby // --
CREATE OR REPLACE FUNCTION platba_potvrdit_prijeti(
      a_platba_pk               platba.platba_pk%TYPE
    , a_castka                  platba.castka_zaplacena%TYPE
    , OUT r_platba_pk           platba.platba_pk%TYPE
    , OUT r_termin_prihlaseni   termin_prihlaseni.termin_prihlaseni_pk%TYPE
) RETURNS RECORD AS $func$
DECLARE
  lr_platba platba%ROWTYPE;

    l_stav platba.stav%TYPE;
BEGIN

    SELECT * INTO lr_platba FROM platba WHERE platba_pk = a_platba_pk;

    IF NOT FOUND THEN
        r_platba_pk := -1;
        r_termin_prihlaseni := -1;
        RETURN;
    END IF;

    l_stav := 'OCEKAVANA';

    IF lr_platba.castka = (coalesce(lr_platba.castka_zaplacena, 0.00) + a_castka) THEN
        l_stav := 'ZAPLACENA';
    END IF;

    r_platba_pk := platba_zmena(
        a_platba_pk,
        lr_platba.castka,
        a_castka,
        now(),
        NULL,
        l_stav,
        lr_platba.kod
    );

    IF l_stav = 'ZAPLACENA' THEN
        UPDATE termin_prihlaseni SET stav = 'POTVRZENY' WHERE platba_pk = a_platba_pk RETURNING termin_prihlaseni_pk INTO r_termin_prihlaseni;
    END IF;

    RETURN;

END;
$func$ LANGUAGE 'plpgsql';
-- //


-- //
-- // dohledam platbu navazanou na moje prihlaseni
-- // pokud neexistuje, vracim -1
-- // pokud je zaplacena, vracim -2
--
-- // ze sveho uctu si vezmu kredity
-- // pokud jich je min nez hodnota platby, tak zustane platba ocekavana
-- // pokud jich je dostatek, rovnou oznacim jako zaplacenou
--
-- // kredity odectu z uzivatelova uctu
-- // vracim platba_pk
CREATE OR REPLACE FUNCTION termin_uplatni_kredity(
      a_prihlaseni_pk termin_prihlaseni.termin_prihlaseni_pk%TYPE
    , a_kreditu platba.castka%TYPE
    , OUT r_platba_pk platba.platba_pk%TYPE
    , OUT r_vysledek TEXT
) RETURNS RECORD AS $func$
DECLARE
    lr_platba platba%ROWTYPE;
    lr_prihlaseni termin_prihlaseni%ROWTYPE;
    lr_uzivatel uzivatel%ROWTYPE;

    l_kredity   uzivatel.ucet_kredity%TYPE;
    l_castka    platba.castka%TYPE;
BEGIN

    SELECT * INTO lr_prihlaseni FROM termin_prihlaseni WHERE termin_prihlaseni_pk = a_prihlaseni_pk;
    IF NOT FOUND THEN
        r_platba_pk := -1;
        r_vysledek := 'Nenalezeno prihlaseni na termin s pk = ' || a_prihlaseni_pk;
        RETURN;
    END IF;

    SELECT * INTO lr_uzivatel FROM uzivatel WHERE uzivatel_pk = lr_prihlaseni.uzivatel_pk;
    IF NOT FOUND THEN
        r_platba_pk := -2;
        r_vysledek := 'Nenalezen uzivatel k prihlaseni na termin s pk = ' || a_prihlaseni_pk;
        RETURN;
    END IF;

    SELECT * INTO lr_platba FROM platba WHERE platba_pk = lr_prihlaseni.platba_pk;
    IF NOT FOUND THEN
        r_platba_pk := -3;
        r_vysledek := 'Nenalazena platba k prihlaseni na termin s pk = ' || a_prihlaseni_pk;
        RETURN;

    -- platba musi byt ocekavana, abych na ni mohl uplatnit kredity
    ELSIF lr_platba.stav <> 'OCEKAVANA' THEN
        r_platba_pk := -3;
        r_vysledek := 'Platba s pk = ' || lr_platba.platba_pk || ' je v neplatnem stavu (' || lr_platba.stav || ')';
        RETURN;

    END IF;

    IF lr_uzivatel.ucet_kredity = 0::NUMERIC OR (a_kreditu IS NOT NULL AND lr_uzivatel.ucet_kredity < a_kreditu) THEN
        r_platba_pk := -4;
        r_vysledek := 'Uzivatel nema dostatek kreditu, ktere by mohl uplatnit';
        RETURN;

    END IF;

    -- kolik kreditu budu platit
    IF a_kreditu IS NOT NULL THEN
        l_castka := a_kreditu;
    ELSE
        l_castka := lr_platba.castka - coalesce(lr_platba.castka_zaplacena, 0.00);
    END IF;

    IF l_castka > lr_uzivatel.ucet_kredity THEN
        l_castka := lr_uzivatel.ucet_kredity;
    END IF;

    -- prijem penez k platbe
    PERFORM platba_potvrdit_prijeti(
        lr_platba.platba_pk,
        l_castka
    );

    PERFORM uzivatel_ucet_zmena(
        lr_uzivatel.uzivatel_pk,
        - l_castka,
        'Uplatneni kreditu',
        lr_platba.platba_pk,
        NULL
    );

    r_platba_pk := lr_prihlaseni.platba_pk;
    r_vysledek := 'OK';

    RETURN;

END;
$func$ LANGUAGE 'plpgsql';