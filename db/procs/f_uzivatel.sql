
-- //
CREATE OR REPLACE FUNCTION uzivatel_nastav_odber(
    a_uzivatel_pk uzivatel.uzivatel_pk%TYPE
  , a_terminy     uzivatel_odber.terminy%TYPE
  , a_novinky     uzivatel_odber.novinky%TYPE
) RETURNS INTEGER AS $func$
DECLARE

BEGIN
  IF NOT EXISTS(SELECT 1 FROM uzivatel WHERE uzivatel_pk = a_uzivatel_pk) THEN
    RETURN -1;
  END IF;

  DELETE FROM uzivatel_odber WHERE uzivatel_pk = a_uzivatel_pk;

  INSERT INTO uzivatel_odber (uzivatel_pk, terminy, novinky) VALUES
    (a_uzivatel_pk, a_terminy, a_novinky);

  RETURN a_uzivatel_pk;
END;
$func$ LANGUAGE 'plpgsql';
-- //

-- // prace s uctem uzivatele, pripisovani a odecitani kreditu
CREATE OR REPLACE FUNCTION uzivatel_ucet_zmena(
      a_uzivatel_pk uzivatel.uzivatel_pk%TYPE
    , a_hodnota historie_uctu.hodnota%TYPE
    , a_popis historie_uctu.popis%TYPE
    , a_platba_pk platba.platba_pk%TYPE
    , a_vlozil uzivatel.uzivatel_pk%TYPE
) RETURNS INTEGER AS $func$
DECLARE
    lr_uzivatel uzivatel%ROWTYPE;

    l_suma uzivatel.ucet_kredity%TYPE;
BEGIN

    SELECT * INTO lr_uzivatel FROM uzivatel WHERE uzivatel_pk = a_uzivatel_pk;
    IF NOT FOUND THEN
        RETURN -1;
    END IF;

    BEGIN
        INSERT INTO historie_uctu (datum, hodnota, popis, uzivatel_pk, platba_pk, vlozil_pk) VALUES
            (now(), a_hodnota, a_popis, a_uzivatel_pk, a_platba_pk, a_vlozil);

        UPDATE uzivatel SET
            ucet_kredity = ucet_kredity + a_hodnota
        WHERE uzivatel_pk = a_uzivatel_pk
        RETURNING * INTO lr_uzivatel;

        SELECT coalesce(sum(hodnota), 0.00) INTO l_suma FROM historie_uctu WHERE uzivatel_pk = a_uzivatel_pk;

        IF l_suma <> lr_uzivatel.ucet_kredity THEN
            RAISE EXCEPTION 'nesedi kontrola sumy historie (%) a uctu uzivatele (%)', l_suma, lr_uzivatel.ucet_kredity;
        END IF;

        RETURN lr_uzivatel.uzivatel_pk;
    EXCEPTION
        WHEN OTHERS THEN
            RETURN -2;
    END;

END;
$func$ LANGUAGE 'plpgsql';
-- //

-- //