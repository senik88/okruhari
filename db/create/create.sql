﻿/*
Created: 31.05.2015
Modified: 25.12.2015
Model: okruhari
Database: PostgreSQL 9.2
*/


-- Create tables section -------------------------------------------------

-- Table uzivatel

CREATE TABLE "uzivatel"(
 "uzivatel_pk" Serial NOT NULL,
 "email" Text NOT NULL,
 "heslo" Text NOT NULL,
 "stav" Text DEFAULT 'AKTIVNI' NOT NULL
        CONSTRAINT "check_stav" CHECK (stav in ('AKTIVNI', 'NEAKTIVNI', 'REGISTRACE')),
 "cas_registrace" Timestamp with time zone NOT NULL,
 "cas_prihlaseni" Timestamp with time zone,
 "jmeno" Text NOT NULL,
 "prijmeni" Text NOT NULL,
 "nick" Text NOT NULL,
 "ucet_kredity" Numeric DEFAULT 0 NOT NULL,
 "validacni_klic" Text NOT NULL,
 "role" Text NOT NULL
)
;

-- Add keys for table uzivatel

ALTER TABLE "uzivatel" ADD CONSTRAINT "Key1" PRIMARY KEY ("uzivatel_pk")
;

ALTER TABLE "uzivatel" ADD CONSTRAINT "email" UNIQUE ("email")
;

ALTER TABLE "uzivatel" ADD CONSTRAINT "validacni_klic" UNIQUE ("validacni_klic")
;

ALTER TABLE "uzivatel" ADD CONSTRAINT "nick" UNIQUE ("nick")
;

-- Table termin

CREATE TABLE "termin"(
 "termin_pk" Serial NOT NULL,
 "cas_od" Timestamp with time zone NOT NULL,
 "cas_do" Timestamp with time zone NOT NULL,
 "zverejnit" Timestamp with time zone,
 "stav" Text NOT NULL,
 "popis" Text NOT NULL,
 "kapacita" Smallint NOT NULL,
 "cena" Numeric(10,2) NOT NULL,
 "uzivatel_pk" Integer NOT NULL
)
;

-- Create indexes for table termin

CREATE INDEX "ix_termin_vypsal" ON "termin" ("uzivatel_pk")
;

-- Add keys for table termin

ALTER TABLE "termin" ADD CONSTRAINT "Key2" PRIMARY KEY ("termin_pk")
;

-- Table termin_prihlaseni

CREATE TABLE "termin_prihlaseni"(
 "termin_prihlaseni_pk" Serial NOT NULL,
 "termin_pk" Integer NOT NULL,
 "uzivatel_pk" Integer NOT NULL,
 "platba_pk" Integer,
 "stav" Text NOT NULL,
 "cas_prihlaseni" Timestamp with time zone NOT NULL
)
;

-- Create indexes for table termin_prihlaseni

CREATE INDEX "ix_termin_termin_prihlaseni" ON "termin_prihlaseni" ("termin_pk")
;

CREATE INDEX "ix_uzivatel_termin_prihlaseni" ON "termin_prihlaseni" ("uzivatel_pk")
;

CREATE INDEX "ix_termin_prihlaseni_platba" ON "termin_prihlaseni" ("platba_pk")
;

-- Add keys for table termin_prihlaseni

ALTER TABLE "termin_prihlaseni" ADD CONSTRAINT "Key3" PRIMARY KEY ("termin_prihlaseni_pk")
;

-- Table platba

CREATE TABLE "platba"(
 "platba_pk" Serial NOT NULL,
 "castka" Numeric(10,2) NOT NULL,
 "castka_zaplacena" Numeric(10,2),
 "datum" Timestamp with time zone,
 "refundace" Numeric(10,2),
 "stav" Text NOT NULL,
 "kod" Integer NOT NULL
)
;

-- Add keys for table platba

ALTER TABLE "platba" ADD CONSTRAINT "Key4" PRIMARY KEY ("platba_pk")
;

ALTER TABLE "platba" ADD CONSTRAINT "kod" UNIQUE ("kod")
;

-- Table mail

CREATE TABLE "mail"(
 "mail_pk" Serial NOT NULL,
 "trida" Text NOT NULL,
 "adresat" Text NOT NULL,
 "predmet" Text NOT NULL,
 "data" Text,
 "cas_vlozeni" Timestamp with time zone DEFAULT now() NOT NULL,
 "cas_naplanovani" Timestamp with time zone NOT NULL,
 "cas_odeslani" Timestamp with time zone,
 "stav" Text NOT NULL
)
;

-- Add keys for table mail

ALTER TABLE "mail" ADD CONSTRAINT "Key5" PRIMARY KEY ("mail_pk")
;

-- Table novinka

CREATE TABLE "novinka"(
 "novinka_pk" Serial NOT NULL,
 "nazev" Text NOT NULL,
 "obsah" Text NOT NULL,
 "stav" Text NOT NULL,
 "cas_vlozeni" Timestamp with time zone NOT NULL,
 "cas_publikovani" Timestamp with time zone,
 "uzivatel_pk" Integer
)
;

-- Create indexes for table novinka

CREATE INDEX "ix_novinka_autor" ON "novinka" ("uzivatel_pk")
;

-- Add keys for table novinka

ALTER TABLE "novinka" ADD CONSTRAINT "Key6" PRIMARY KEY ("novinka_pk")
;

-- Table uzivatel_odber

CREATE TABLE "uzivatel_odber"(
 "uzivatel_pk" Integer NOT NULL,
 "terminy" Boolean DEFAULT true,
 "novinky" Boolean DEFAULT false
)
;

-- Create indexes for table uzivatel_odber

CREATE INDEX "ix_uzivatel_odber" ON "uzivatel_odber" ("uzivatel_pk")
;

-- Add keys for table uzivatel_odber

ALTER TABLE "uzivatel_odber" ADD CONSTRAINT "Key7" PRIMARY KEY ("uzivatel_pk")
;

-- Table pocasi

CREATE TABLE "pocasi"(
 "pocasi_pk" Serial NOT NULL,
 "mesto" Text NOT NULL,
 "den" Date NOT NULL,
 "zdroj" Text NOT NULL,
 "teplota_denni" Numeric NOT NULL,
 "teplota_max" Numeric,
 "teplota_min" Numeric,
 "vlhkost" Numeric NOT NULL,
 "tlak" Integer NOT NULL,
 "vitr_rychlost" Numeric NOT NULL,
 "vitr_smer" Numeric NOT NULL,
 "dest" Numeric NOT NULL,
 "mraky" Integer NOT NULL,
 "platne_do" Timestamp with time zone NOT NULL
)
;

-- Add keys for table pocasi

ALTER TABLE "pocasi" ADD CONSTRAINT "Key8" PRIMARY KEY ("pocasi_pk")
;

-- Table soubor

CREATE TABLE "soubor"(
 "soubor_pk" Serial NOT NULL,
 "nazev" Text NOT NULL,
 "velikost" Numeric NOT NULL,
 "typ" Text NOT NULL,
 "hash" Text NOT NULL,
 "popis" Text,
 "uzivatel_pk" Integer,
 "pocet_stazeni" Integer DEFAULT 0 NOT NULL
)
;

-- Create indexes for table soubor

CREATE INDEX "IX_Relationship3" ON "soubor" ("uzivatel_pk")
;

-- Add keys for table soubor

ALTER TABLE "soubor" ADD CONSTRAINT "Key9" PRIMARY KEY ("soubor_pk")
;

-- Table termin_soubor

CREATE TABLE "termin_soubor"(
 "soubor_pk" Integer NOT NULL,
 "termin_pk" Integer NOT NULL
)
;

-- Create indexes for table termin_soubor

CREATE INDEX "IX_Relationship1" ON "termin_soubor" ("soubor_pk")
;

CREATE INDEX "IX_Relationship2" ON "termin_soubor" ("termin_pk")
;

-- Add keys for table termin_soubor

ALTER TABLE "termin_soubor" ADD CONSTRAINT "Key10" PRIMARY KEY ("soubor_pk","termin_pk")
;

-- Table historie_uctu

CREATE TABLE "historie_uctu"(
 "historie_uctu_pk" Serial NOT NULL,
 "datum" Timestamp with time zone NOT NULL,
 "hodnota" Numeric NOT NULL,
 "popis" Text NOT NULL,
 "uzivatel_pk" Integer,
 "vlozil_pk" Integer,
 "platba_pk" Integer
)
;

-- Create indexes for table historie_uctu

CREATE INDEX "ix_uzivatel_historie_uctu" ON "historie_uctu" ("uzivatel_pk")
;

CREATE INDEX "ix_vlozil_historie_uctu" ON "historie_uctu" ("vlozil_pk")
;

CREATE INDEX "IX_Relationship6" ON "historie_uctu" ("platba_pk")
;

-- Add keys for table historie_uctu

ALTER TABLE "historie_uctu" ADD CONSTRAINT "Key11" PRIMARY KEY ("historie_uctu_pk")
;

-- Create relationships section ------------------------------------------------- 

ALTER TABLE "termin_prihlaseni" ADD CONSTRAINT "termin_termin_prihlaseni" FOREIGN KEY ("termin_pk") REFERENCES "termin" ("termin_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "termin_prihlaseni" ADD CONSTRAINT "uzivatel_termin_prihlaseni" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "termin_prihlaseni" ADD CONSTRAINT "termin_prihlaseni_platba" FOREIGN KEY ("platba_pk") REFERENCES "platba" ("platba_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "novinka" ADD CONSTRAINT "uzivatel_novinka" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "uzivatel_odber" ADD CONSTRAINT "uzivatel_odber" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "termin" ADD CONSTRAINT "termin_vypsal" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "termin_soubor" ADD CONSTRAINT "Relationship1" FOREIGN KEY ("soubor_pk") REFERENCES "soubor" ("soubor_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "termin_soubor" ADD CONSTRAINT "Relationship2" FOREIGN KEY ("termin_pk") REFERENCES "termin" ("termin_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "soubor" ADD CONSTRAINT "Relationship3" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "historie_uctu" ADD CONSTRAINT "uzivatel_historie_uctu" FOREIGN KEY ("uzivatel_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "historie_uctu" ADD CONSTRAINT "vlozil_historie_uctu" FOREIGN KEY ("vlozil_pk") REFERENCES "uzivatel" ("uzivatel_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;

ALTER TABLE "historie_uctu" ADD CONSTRAINT "platba_historie_uctu" FOREIGN KEY ("platba_pk") REFERENCES "platba" ("platba_pk") ON DELETE NO ACTION ON UPDATE NO ACTION
;





