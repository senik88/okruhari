-- uzivatele
insert into uzivatel (email, heslo, role, stav, validacni_klic, cas_registrace, cas_prihlaseni, jmeno, prijmeni, nick) values
    ('superadmin', '$2y$13$uS9gezAySu8bCHJaPbeEguklV7cwqVf/99eF4Aaj4uL7Ll2upSmeu', 'superadmin', 'AKTIVNI', md5('superadmin'), now(), null, 'Super', 'Admin', 'superadmin')
  , ('test@senovo.cz', '$2y$13$uS9gezAySu8bCHJaPbeEguklV7cwqVf/99eF4Aaj4uL7Ll2upSmeu', 'admin', 'AKTIVNI', md5('test@senovo.cz'), now(), null, 'Test', 'Senovo', 'senovo')
;

-- prihlaseni odberu
insert into uzivatel_odber (uzivatel_pk, terminy, novinky) VALUES
    (1, true, true)
  , (2, true, true)
;